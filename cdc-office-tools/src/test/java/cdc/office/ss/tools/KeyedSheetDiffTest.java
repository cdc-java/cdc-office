package cdc.office.ss.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import cdc.office.csv.CsvWriter;
import cdc.office.tables.Row;
import cdc.office.tools.KeyedSheetDiff;
import cdc.util.cli.MainResult;
import cdc.util.files.Files;

class KeyedSheetDiffTest {
    private static CsvBuilder csv(String filename) {
        return new CsvBuilder(filename);
    }

    private static class CsvBuilder {
        private final String filename;
        private final List<Row> rows = new ArrayList<>();

        public CsvBuilder(String filename) {
            this.filename = filename;
        }

        public CsvBuilder row(String... values) {
            rows.add(Row.builder().addValues(values).build());
            return this;
        }

        public File build() throws IOException {
            final File file = new File("target", filename + ".csv");
            try (final CsvWriter writer = new CsvWriter(file, StandardCharsets.UTF_8)) {
                writer.setSeparator(';');
                boolean first = true;
                for (final Row row : rows) {
                    if (first) {
                        first = false;
                    } else {
                        writer.writeln();
                    }
                    writer.write(row.getValues());
                }
                writer.flush();
                return file;
            }
        }
    }

    private static void compare(String basename,
                                Consumer<CsvBuilder> csv1Consumer,
                                Consumer<CsvBuilder> csv2Consumer,
                                Consumer<KeyedSheetDiff.MainArgs> margsConsumer,
                                Consumer<CsvBuilder> csveConsumer) throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();

        final CsvBuilder csv1 = csv(basename + ".1");
        csv1Consumer.accept(csv1);
        margs.file1 = csv1.build();

        final CsvBuilder csv2 = csv(basename + ".2");
        csv2Consumer.accept(csv2);
        margs.file2 = csv2.build();

        margs.output = new File("target", basename + ".o.csv");

        margsConsumer.accept(margs);

        final CsvBuilder csve = csv(basename + ".e");
        csveConsumer.accept(csve);
        final File e = csve.build();

        KeyedSheetDiff.execute(margs);

        final String expected = FileUtils.readFileToString(e, StandardCharsets.UTF_8);
        final String actual = FileUtils.readFileToString(margs.output, StandardCharsets.UTF_8);

        assertEquals(expected, actual);
    }

    private static void check(String filename1,
                              String filename2,
                              String ext,
                              boolean lineMarkColumn,
                              KeyedSheetDiff.MainArgs.Feature... features) throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File(filename1);
        margs.file2 = new File(filename2);

        final String ext1 = Files.getExtension(margs.file1);
        final String ext2 = Files.getExtension(margs.file2);

        margs.output = new File("target",
                                "diff-" + ext1 + "-" + ext2
                                        + "-" + (lineMarkColumn ? "line-mark" : "no-line-mark")
                                        + toString(features)
                                        + "." + ext);
        margs.keys.add("ID");
        margs.features.addAll(features);
        margs.diffMark = lineMarkColumn ? "Diff" : null;
        KeyedSheetDiff.execute(margs);
    }

    private static String toString(KeyedSheetDiff.MainArgs.Feature... features) {
        final StringBuilder builder = new StringBuilder();
        for (final KeyedSheetDiff.MainArgs.Feature feature : features) {
            builder.append("-");
            builder.append(feature.name().toLowerCase());
        }
        return builder.toString();
    }

    @Test
    void testNonMatchingHeaders() throws IOException {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("src/test/resources/ksd-test1-file1.csv",
                               "src/test/resources/ksd-test1-file2.csv",
                               "csv",
                               false);
                     });
    }

    @Test
    void testInvalidHeader() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-invalid-header-file1.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-invalid-header-file2.xlsx");
        margs.output = new File("target", "ksd-test-invalid-header.xlsx");
        margs.keys.add("ID");
        margs.sheet1 = "Sheet";
        margs.sheet2 = "Sheet";
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.VERBOSE, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.FIX_HEADERS, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        KeyedSheetDiff.execute(margs);
        assertTrue(true);
    }

    @Test
    void testDuplicateKeys() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-duplicate-keys-file1.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-duplicate-keys-file2.xlsx");
        margs.output = new File("target", "ksd-test-duplicate-keys.xlsx");
        margs.keys.add("ID");
        margs.sheet1 = "Sheet";
        margs.sheet2 = "Sheet";
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.VERBOSE, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.SAVE_SYNTHESIS, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.IGNORE_DUPLICATES, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        KeyedSheetDiff.execute(margs);
        assertTrue(true);
    }

    @Test
    void testInvalidSheet1() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-invalid-sheet-file1.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-invalid-sheet-file2.xlsx");
        margs.output = new File("target", "ksd-test-invalid-sheet.xlsx");
        margs.keys.add("ID");
        margs.sheet1 = "XXX";
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.VERBOSE, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        assertThrows(IOException.class,
                     () -> KeyedSheetDiff.execute(margs));
    }

    @Test
    void testInvalidSheet2() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-invalid-sheet-file1.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-invalid-sheet-file2.xlsx");
        margs.output = new File("target", "ksd-test-invalid-sheet.xlsx");
        margs.keys.add("ID");
        margs.sheet2 = "XXX";
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.VERBOSE, true);
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        assertThrows(IOException.class,
                     () -> KeyedSheetDiff.execute(margs));
    }

    @Test
    void testEmptySheet1() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-empty-sheet.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-non-empty-sheet.xlsx");
        margs.output = new File("target", "ksd-test-empty-sheet.xlsx");
        margs.keys.add("ID");
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        assertThrows(IllegalArgumentException.class,
                     () -> KeyedSheetDiff.execute(margs));
    }

    @Test
    void testEmptySheet2() throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.file1 = new File("src/test/resources/ksd-test-non-empty-sheet.xlsx");
        margs.file2 = new File("src/test/resources/ksd-test-empty-sheet.xlsx");
        margs.output = new File("target", "ksd-test-empty-sheet.xlsx");
        margs.keys.add("ID");
        margs.features.setEnabled(KeyedSheetDiff.MainArgs.Feature.ZIP32, true);
        assertThrows(IllegalArgumentException.class,
                     () -> KeyedSheetDiff.execute(margs));
    }

    @Test
    void testEmpty() throws IOException {
        compare("empty",
                csv -> {
                    csv.row("ID", "A", "B");
                },
                csv -> {
                    csv.row("ID", "A", "B");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_LINE_DIFF_COLUMN);
                },
                csv -> {
                    csv.row("ID", "A", "B");
                });
    }

    @Test
    void testSame() throws IOException {
        compare("same",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B")
                       .row("SAME", "1", "x", "y");
                });
    }

    @Test
    void testRemove() throws IOException {
        compare("remove",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B")
                       .row("REMOVED", "<R>1", "<R>x", "<R>y");
                });
    }

    @Test
    void testAdd() throws IOException {
        compare("add",
                csv -> {
                    csv.row("ID", "A", "B");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B")
                       .row("ADDED", "<A>1", "<A>x", "<A>y");
                });
    }

    @Test
    void testChange() throws IOException {
        compare("change",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "X", "Y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B")
                       .row("CHANGED", "1", "<C>X", "<C>Y");
                });
    }

    @Test
    void testData() throws IOException {
        compare("data",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "x", "y")
                       .row("3", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "Y")
                       .row("2", "X", "Y")
                       .row("4", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.attributes.add("A");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A")
                       .row("SAME", "1", "x")
                       .row("CHANGED", "2", "<C>X")
                       .row("REMOVED", "<R>3", "<R>x")
                       .row("ADDED", "<A>4", "<A>x");
                });
    }

    @Test
    void testAddedColumn() throws IOException {
        compare("added-column",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B", "C")
                       .row("1", "x", "y", "z");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B", "C")
                       .row("CHANGED", "1", "x", "y", "<A>z");
                });
    }

    @Test
    void testRemovedColumn() throws IOException {
        compare("removed-column",
                csv -> {
                    csv.row("ID", "A", "B", "C")
                       .row("1", "x", "y", "z");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y", "z");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "A", "B", "C")
                       .row("CHANGED", "1", "x", "y", "<R>z");
                });
    }

    @Test
    void testMap() throws IOException {
        compare("map",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "x", "y")
                       .row("3", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "X", "Y")
                       .row("4", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.map1.put("A", "a");
                    margs.map2.put("A", "a");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID", "a", "B")
                       .row("SAME", "1", "x", "y")
                       .row("CHANGED", "2", "<C>X", "<C>Y")
                       .row("REMOVED", "<R>3", "<R>x", "<R>y")
                       .row("ADDED", "<A>4", "<A>x", "<A>y");
                });
    }

    @Test
    void testSplitComparisonsNoMarks() throws IOException {
        compare("split-comparisons-no-marks",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "x", "y")
                       .row("3", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "X", "Y")
                       .row("4", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_MARKS);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID 1", "ID 2", "A 1", "A 2", "B 1", "B 2")
                       .row("SAME", "1", "1", "x", "x", "y", "y")
                       .row("CHANGED", "2", "2", "x", "X", "y", "Y")
                       .row("REMOVED", "3", "", "x", "", "y")
                       .row("ADDED", "", "4", "", "x", "", "y");
                });
    }

    @Test
    void testSplitComparisonsNoMarksAddCellDiff() throws IOException {
        compare("split-comparisons-no-marks-cell-diff",
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "x", "y")
                       .row("3", "x", "y");
                },
                csv -> {
                    csv.row("ID", "A", "B")
                       .row("1", "x", "y")
                       .row("2", "X", "Y")
                       .row("4", "x", "y");
                },
                margs -> {
                    margs.keys.add("ID");
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS);
                    margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_MARKS);
                    margs.diffMark = "DIFF";
                },
                csv -> {
                    csv.row("DIFF", "ID DIFF", "ID 1", "ID 2", "A DIFF", "A 1", "A 2", "B DIFF", "B 1", "B 2")
                       .row("SAME", "SAME", "1", "1", "SAME", "x", "x", "SAME", "y", "y")
                       .row("CHANGED", "SAME", "2", "2", "CHANGED", "x", "X", "CHANGED", "y", "Y")
                       .row("REMOVED", "REMOVED", "3", "", "REMOVED", "x", "", "REMOVED", "y")
                       .row("ADDED", "ADDED", "", "4", "ADDED", "", "x", "ADDED", "", "y");
                });
    }

    @Test
    void testIssue42() {
        assertSame(MainResult.SUCCESS,
                   KeyedSheetDiff.exec("--args-file",
                                       "src/test/resources/issue42-args.txt"));
    }

    @Test
    void testIssue48() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         KeyedSheetDiff.exec("--args-file",
                                             "src/test/resources/issue48-args.txt");
                     });
        assertSame(MainResult.SUCCESS,
                   KeyedSheetDiff.exec("--args-file",
                                       "src/test/resources/issue48-args.txt",
                                       "--fix-headers"));
    }

    @Test
    void testIssue57() {
        assertThrows(RuntimeException.class,
                     () -> {
                         KeyedSheetDiff.exec("--args-file",
                                             "src/test/resources/issue57-args.txt",
                                             "--strict-csv");
                     });
        assertSame(MainResult.SUCCESS,
                   KeyedSheetDiff.exec("--args-file",
                                       "src/test/resources/issue57-args.txt"));
    }

    @Test
    void testIssue59() {
        assertSame(MainResult.SUCCESS,
                   KeyedSheetDiff.exec("--args-file",
                                       "src/test/resources/issue59-args.txt"));
    }

    @Test
    void testMultiSheets() throws IOException {
        final MainResult result =
                KeyedSheetDiff.exec("--file1",
                                    "src/test/resources/ksd-test-multi-1.xlsx",
                                    "--pattern1",
                                    "Sheet#[0-9]*",
                                    "--file2",
                                    "src/test/resources/ksd-test-multi-2.xlsx",
                                    "--pattern2",
                                    "Sheet#[0-9]*",
                                    "--output",
                                    "target/ksd-test-multi.xlsx",
                                    "--key",
                                    "ID",
                                    "--verbose",
                                    "--zip32");
        assertSame(MainResult.SUCCESS, result);
    }

    @Test
    void testMultiSheetsNoMatch1() throws IOException {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         KeyedSheetDiff.exec("--file1",
                                             "src/test/resources/ksd-test-multi-1.xlsx",
                                             "--pattern1",
                                             "Bad",
                                             "--file2",
                                             "src/test/resources/ksd-test-multi-2.xlsx",
                                             "--pattern2",
                                             "Sheet#[0-9]*",
                                             "--output",
                                             "target/ksd-test-multi-no-match1.xlsx",
                                             "--key",
                                             "ID",
                                             "--verbose");
                     });
    }

    @Test
    void testMultiSheetsNoMatch2() throws IOException {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         KeyedSheetDiff.exec("--file1",
                                             "src/test/resources/ksd-test-multi-1.xlsx",
                                             "--pattern1",
                                             "Sheet#[0-9]*",
                                             "--file2",
                                             "src/test/resources/ksd-test-multi-2.xlsx",
                                             "--pattern2",
                                             "Bad",
                                             "--output",
                                             "target/ksd-test-multi-no-match2.xlsx",
                                             "--key",
                                             "ID",
                                             "--verbose");
                     });
    }

    @Test
    void testMultiInvalidPattern1() throws IOException {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         KeyedSheetDiff.exec("--file1",
                                             "src/test/resources/ksd-test-multi-1.xlsx",
                                             "--pattern1",
                                             "Bad[",
                                             "--file2",
                                             "src/test/resources/ksd-test-multi-2.xlsx",
                                             "--pattern2",
                                             "Sheet#[0-9]*",
                                             "--output",
                                             "target/ksd-test-multi-invalid-pattern1.xlsx",
                                             "--key",
                                             "ID",
                                             "--verbose");
                     });
    }

}