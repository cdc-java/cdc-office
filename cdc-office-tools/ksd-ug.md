# Keyed Sheet Diff

**Keyed Sheet Diff** (*aka* **KSD**) can compare 2 sets of workbook sheets whose lines are identified by one or more columns (key columns).  

## Principles
The **first file** is specified with the `--file1` option.  
The **first sheet(s)** can be specified with the `--sheet1` or `--pattern1` option, otherwise the first sheet of first file is compared.

The **second file** is specified with the `--file2` option.  
The **second sheet(s)** can be specified with the `--sheet2` or `--pattern2` option, otherwise the first sheet of second file is compared.

The `--pattern1` and `--pattern2` options expect a Java pattern to load and merge several sheet.
Typical examples:
- `.*` to match all sheets.
- `Foo.*` to match all sheets whose name starts with `Foo` followed by any number of characters.
- `Foo[0-9]` to match all sheets whose name starts with `Foo` followed by 1 digit.
- `Foo[0-9]+` to match all sheets whose name starts with `Foo` followed by at least 1 digit.
- `Foo[0-9]*` to match all sheets whose name starts with `Foo` followed by any number of  digits.
- `Foo` to match the sheet named `Foo`.
- `Foo|Bar` to match the sheets named `Foo` and `Bar`.


The **result file** is specified  with the `--output` option.  
The `--max-rows` option can be used to control the number of rows in output sheet(s).
By default, output sheets are filled up to the maximum supported number of rows.
When an output sheet is full, a new one is added.

One or several sheets can be loaded from a workbook.  
When several sheets of a workbook are loaded, they must all have the same header (same columns in same order).  
Loading several sheets can be used when rows to compare don't hold in one sheet because of format limitations.

![Image](doc-files/ksd-inputs.svg)

Each input sheet contains one header row and any number of data rows.

![Image](doc-files/ksd-sheets.svg)

**Note:** on above diagram, keys columns are grouped first, but they can be anywhere.

### Header row
The first row of each input sheet must be a header containing names of columns.  
Those names must be unique. They are case sensitive. Their order can differ between first and second file.  
All loaded sheets of a workbook must have the same header.  
All columns that have a name or are followed by a column that has a name are taken into account.  
It is an error to have a column without name followed by a column with a name.  
Original column names (keys and attributes) can be modified using `--map1` and `--map2` options.

- **Key columns**  
One or more columns (defined with the `--key` option) are used to identify each row.  
If a key column is missing in an input sheet, comparison will fail.
- **Attribute columns**  
Other column names may differ. They should generally be identical to obtain a meaningful comparison.  
If necessary, the names of attributes columns to compare can be specified with `--attribute` option. If this option is absent, all attributes columns are compared.

### Data rows
In both sheets, each data row must have a unique composite key.  
If, in a sheet, 2 rows (or more) are identified by the same composite key, comparison will fail.  
The order of rows can differ between first and second file.  
When a data row contains more columns than the header, data columns in excess are ignored.

### Comparison of data rows
- If a composite key is only present in the first sheet, the row is considered as removed.
- If a composite key is only present in the second sheet, the row is considered as added.
- If a composite key is present in both sheets, rows are either identical or different. A comparison of cells is done.

#### Comparison of attribute cells
Attribute cells are compared when their column names are the same:
- If a column name is only present in the first sheet, the cell is considered as removed.
- If a column name is only present in the second sheet, the cell is considered as added.
- If a column name is present in both sheets, cell contents are compared.

If all attribute cells of matching data rows have identical contents, the 2 rows are identical.  
Otherwise, the 2 rows differ.

### Comparison result
Comparison of rows can be displayed in a dedicated column, the **line diff column**, that is added to input columns.  
By default, this column is shown. It can be hidden with `--no-line-diif-column`.  
The name of the line diff column can be set with the `--diff-mark` option.  
If present, the content of this column will be:
- **ADDED** for an added row (only present in second sheet),
- **CHANGED** for a changed rows,
- **REMOVED** for a removed row (only present in first sheet),
- **SAME** for an identical rows.

Comparison of attribute cells can be displayed using marks or colors.  
Marks are used by default with output formats that don't support colors.  
Colors are used by default with output formats that support them:
- $`\textcolor{red}{\text{red}}`$  is used for removed rows and cells,
- $`\textcolor{blue}{\text{blue}}`$ is used for added rows and cells,
- $`\textcolor{magenta}{\text{magenta}}`$ is used for changed rows and cells.
- $`\textcolor{black}{\text{black}}`$ is used for identical rows and cells.

Marks can be customized (using `--added-mark`, `--changed-mark`, `--removed-mark` and `--unchanged-mark` options).  
Default marks are:
- **<A>** for added cells,
- **<C>** for changed cells,
- **<R>** for removed cells,
- and nothing for identical cells.

Using `--cell-diff-columns`, will add a **diff column** for each input column.  
If present, the content of those columns will be:
- **ADDED** for an added cell,
- **CHANGED** for a changed cells,
- **REMOVED** for a removed cell,
- **SAME** or **NULL** for an identical cells. `NULL` is used when both cells are empty.

Using `--split-comparisons` will create 2 columns for each compared input column,
one with the content of the first file, the other with the content of the second file.  

Identical rows can be skipped by using the `--no-unchanged-lines` option.  

Marking of added and removed cells can be skipped using the `--no-added-or-removed-marks`.

Using `--show-original-names`, optionaly combined  with `--file1-mark` and `--file2-mark`,  will
add the original names in header of output file.

By default, first rows of result file are are those of the second file, completed with removed rows.  
If the `--sort-lines` option is used, then rows of result file are sorted using their composite key.
The order of declaration of keys matters in that case.

### Synthesis
A synthesis of comparison can be:
- displayed on console (using `--synthesis` option)
- inserted in an additional sheet of the output file (using `--save-synthesis` option).

## Formats
**CSV**, **XLS**, **XLSX**, **XLSM** and **ODS** are supported as input and output formats.  
The compared sheets don't need to use the same format.  
However, using the same input format for both sheets may give better results.

The output format may differ from input format(s).

**Warning:** **ODS** output format has currently more limitations than `Office` formats, and is slower to process.

When using **CSV**, one can optionally specify:
- the used separator, using the `--separator` option,
- the file character set, using the `--charset` option.

**Warning:** all **CSV** files must use the same separator and character set.


## Examples
The 2 input sheets are:  
![Image](doc-files/ksd-file1.png) ![Image](doc-files/ksd-file2.png)

There is 1 `key` column: ID.  
There are 4 `attribute` columns: A, B, C and D.

Minimal mandatory options, if the sheets to compare are the first ones, are:
`--file1` &lt;file1> `--file2` &lt;file2> `--output` &lt;output> `--key` ID  
Otherwise, one must add `--sheet1` &lt;sheet1> option if the sheet of first file &lt;file1> is not the first one,
and `--sheet2` &lt;sheet2> option if the sheet of second file &lt;file2> is not the first one.

Depending on additional options and on output format, result will be:

<table>
   <thead>
      <tr><th>Additional options</th><th>Outputs</th><th>Notes</th></tr>
   </thead>
   <tbody>
      <tr>
         <td><code></code></td>
         <td><img src="doc-files/ksd-output-default-office.png"/><img src="doc-files/ksd-output-default-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--no-line-diff-column</code></td>
         <td><img src="doc-files/ksd-output-nldc-office.png"/><img src="doc-files/ksd-output-nldc-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--attribute</code> A B</td>
         <td><img src="doc-files/ksd-output-att-office.png"/><img src="doc-files/ksd-output-att-csv.png"/></td>
         <td>Only A and B columns are compared.</td>
      </tr>
      <tr>
         <td><code>--key</code> id<br><code>--map1</code> ID::id A::a B::b<br><code>--map2</code> ID::id A::a B::b</td>
         <td><img src="doc-files/ksd-output-map-office.png"/><img src="doc-files/ksd-output-map-csv.png"/></td>
         <td>ID, A and B columns are renamed.</td>
      </tr>
      <tr>
         <td><code>--no-added-or-removed-marks</code></td>
         <td><img src="doc-files/ksd-output-naorm-office.png"/><img src="doc-files/ksd-output-naorm-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--diff-mark</code> DIFF</td>
         <td><img src="doc-files/ksd-output-dm-office.png"/><img src="doc-files/ksd-output-dm-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--show-change-details</code></td>
         <td><img src="doc-files/ksd-output-scd-office.png"/><img src="doc-files/ksd-output-scd-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--show-change-details<br><code>--no-unchanged-lines<br>--sort-lines</code></td>
         <td><img src="doc-files/ksd-output-scd-nul-sl-office.png"/><img src="doc-files/ksd-output-scd-nul-sl-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--show-change-details<br><code>--no-unchanged-lines<br>--sort-lines<br>--no-added-or-removed-marks</code></td>
         <td><img src="doc-files/ksd-output-scd-nul-sl-naorm-office.png"/><img src="doc-files/ksd-output-scd-nul-sl-naorm-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--split-comparisons</code></td>
         <td><img src="doc-files/ksd-output-sc-office.png"/><br><img src="doc-files/ksd-output-sc-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--cell-diff-columns</code></td>
         <td><img src="doc-files/ksd-output-cdc-office.png"/><br><img src="doc-files/ksd-output-cdc-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--cell-diff-columns<br>--show-change-details</code></td>
         <td><img src="doc-files/ksd-output-cdc-scd-office.png"/><br><img src="doc-files/ksd-output-cdc-scd-csv.png"/></td>
         <td></td>
      </tr>
      <tr>
         <td><code>--split-comparisons<br>--cell-diff-columns<br>--original-names</code><br><code>--file1-mark</code> (File 1)<br><code>--file2-mark</code> (File 2)</td>
         <td><img src="doc-files/ksd-output-sc-cdc-on-office.png"/><br><img src="doc-files/ksd-output-sc-cdc-on-csv.png"/></td>
         <td><code>--original-names</code> option is useless as there is no name mapping.</td>
      </tr>
   </tbody>
</table>

If the `--save-synthesis` option is enabled, an additional sheet is generated with that content:

![Image](doc-files/ksd-output-synthesis.png)

- `Lines` gives the count of the 4 possible values in `line mark column` (**Diff** or **Line Diff** in above example), even if unchanged lines are filtered out.
- `Cells` gives the count for all cells, including added and removed lines.
- One line is created for each column. In the above example, there are 5 columns.


## Common errors

![Image](doc-files/ksd-processing.svg)


The following table lists commons errors, their symptoms and solutions.

<table>
   <thead>
      <tr><th>Symptoms</th><th>Step</th><th>Cause</th><th>Solution</th></tr>
   </thead>
   <tbody>
      <tr>
         <td>KSD fails with an exception like:<br><code>... Zip bomb detected ...</code></td>
         <td>2</td>
         <td>The input file would exceed the maximum allowed ratio of expanded file size to the compressed file size.<br>
         This maximum ratio depends on file format. It is typically 100 with XLSX.</td>
         <td>Clean the input file or add <code>--no-vulnerability-detection</code> option.</td>
      </tr>
      <tr>
         <td>KSD fails with an exception like:<br><code>Invalid sheet name: ...</code></td>
         <td>2</td>
         <td>Sheets with expected name <b>MUST</b> exits.<br>They don't.</td>
         <td>Make sure sheet names correspond to existing sheets.</td>
      </tr>
      <tr>
         <td>KSD fails with an exception like:<br><code>Invalid sheet name pattern: ...</code></td>
         <td>2</td>
         <td>The pattern <b>MUST</b> be a valid Java pattern.<br>It is not.</td>
         <td>Make sure pattern is valid.</td>
      </tr>
      <tr>
         <td>KSD fails with an exception like:<br><code>file1/file2 does not contain any sheet matching '...'.</code></td>
         <td>2</td>
         <td>The pattern <b>MUST</b> must match at least one sheet name.<br>It does not.</td>
         <td>Make sure pattern matches at least one sheet name.</td>
      </tr>
      <tr>
         <td>KSD fails with an exception like:<br><code>No data in file1/file2 sheet.</code></td>
         <td>2</td>
         <td>Sheets <b>MUST</b> have a content, at least one row for header.<br>They don't.</td>
         <td>Make sure sheets contain at least one line, the first one being the header.</td>
      </tr>
      <tr>
         <td>KSD fails with an exception like:<br><code>Header of file1/file2 sheet contains empty intermediate cell(s): ...</code></td>
         <td>2</td>
         <td>In headers, all cells but the last ones <b>MUST</b> have a name.<br>An empty header cell is followed by a non-empty one.</td>
         <td>Make sure only last columns have no name.<br>One can temporarily enable <code>--fix-headers</code>.</td>
      </tr>
      <tr>
         <td>KSD probably fails with an exception like:<br><code>Missing keys: [...] in file1/file2 header: [...]</code></td>
         <td>2</td>
         <td>Both input CSV files <b>MUST</b> use the same separator and character set.<br>They don't.</td>
         <td>Convert one or both files in order to use the same separator and character set.</td>
      </tr>
      <tr>
         <td>KSD probably fails with an exception like:<br><code>Missing keys: [...] in file1/file2 header: [...]</code></td>
         <td>2</td>
         <td>Input CSV file(s) <b>MUST</b> be compliant with the default or specified separator and character set.<br>It isn't / they aren't.<br></td>
         <td>Convert CSV file(s) to use the specified separator and character set,<br>or specify correct separator and character set.</td>
      </tr>
      <tr>
         <td>KSD fails.<br>An exception is thrown with a message similar to:<br><code>Missing keys: [...] in file1/file2 header: [...]</code></td>
         <td>3</td>
         <td>Headers <b>MUST</b> be compliant with specified keys.<br>They aren't.</td>
         <td>Fix key names in input files or command line, or add missing key column(s) with appropriate name(s).</td>
      </tr>
      <tr>
         <td>KSD fails.<br>An exception is thrown with a message similar to:<br><code>Invalid header (duplicate names): [...]</code></td>
         <td>3</td>
         <td>Header names <b>MUST</b> be unique.<br>They aren't.</td>
         <td>Fix header names so that they are all different.<br>One can temporarily enable <code>--fix-headers</code>.</td>
      </tr>
      <tr>
         <td>KSD fails.<br>An exception is thrown with a message similar to:<br><code>Duplicate key [...] in (LEFT|RIGHT) row [...], line ...</code></td>
         <td>3</td>
         <td>Identifiers <b>MUST</b> be unique.<br>They aren't.</td>
         <td>Remove lines with duplicate identifiers.<br>One can temporarily enable <code>--ignore-duplicates</code>.</td>
      </tr>
      <tr>
         <td>KSD succeeds.<br>Many or all attribute cells are marked as <code>ADDED</code> or <code>REMOVED</code>.</td>
         <td>4</td>
         <td>Headers <b>ARE</b> case sensitive. Attributes of headers <b>MUST</b> be consistent.<br>They aren't.</td>
         <td>Make sure both headers use the same names and case.</td>
      </tr>
   </tbody>
</table>


Two options are usefull when input files contain errors, and should be used the first time a comparison is done:
- `--fix-headers`:  If headers contain empty or duplicate names, they are renamed as `Column#`.  
  Comparison will very probably be irrelevant for columns that have been renamed.
- `--ignore-duplicates`: If there are duplicate rows, they are ignored. Comparison is done with the first row that has a key.  
  Comparison will very probably be irrelevant for rows that have duplicates.

**Warning:** These options should not be enabled permanently, otherwise comparison may seem valid when it is not.

## Options
Options of KSD are:

```
USAGE
KeyedSheetDiff [--added-mark <arg>] [--added-or-removed-marks | --no-added-or-removed-marks]
                     [--args-file <arg>] [--args-file-charset <arg>] [--attribute <arg>]
                     [--auto-size-columns] [--auto-size-rows] [--cell-diff-columns |
                     --no-cell-diff-columns] [--changed-mark <arg>] [--charset <arg>] [--charset1
                     <arg>] [--charset2 <arg>] [--colors | --no-colors] [--diff-mark <arg>] --file1
                     <arg> [--file1-mark <arg>] --file2 <arg> [--file2-mark <arg>] [--fix-headers]
                     [-h | -v] [--help-width <arg>] [--ignore-duplicates] --key <arg>
                     [--line-diff-column | --no-line-diff-column] [--map1 <arg>] [--map2 <arg>]
                     [--marks | --no-marks] [--max-rows <arg>]      [--no-strict-csv | --strict-csv]
                     [--no-unchanged-lines | --unchanged-lines] [--no-vulnerability-protections |
                     --vulnerability-protections] --output <arg> [--pattern1 <arg> | --sheet1 <arg>]
                     [--pattern2 <arg> | --sheet2 <arg>] [--removed-mark <arg>] [--save-synthesis]
                     [--separator <arg>] [--separator1 <arg>] [--separator2 <arg>] [--sheet <arg>]
                     [--show-change-details] [--show-original-names] [--sort-lines]
                     [--split-comparisons]  [--synthesis]  [--unchanged-mark <arg>]  [--verbose]
                     [--zip32 | --zip64]

KeyedSheetDiff is used to compare two sets of sheets in workbooks (csv, xls, xlsx, xlsm  or ods).
If several sheets are loaded from a file, they must all have the same columns, in the same order.
Lines in sheets are matched by a set of key columns.
Key and attribute columns can be renamed.
The attribute columns to compare can be selected.
Input and output files can use different formats.
Differences are indicated with textual marks or colors (if output format supports it).

'--sheet1' (resp. '--sheet2') and '--pattern1' (resp. '--pattern2') options are exclusive and
optional.

Patterns are Java patterns.

OPTIONS
    --added-mark <arg>               Optional mark for added cells (default: "<A>").
    --added-or-removed-marks         Output added or removed marks (default).
    --args-file <arg>                Optional name of the file from which options can be read.
                                     A line is either ignored or interpreted as a single argument
                                     (option or value).
                                     A line is ignored when it is empty or starts by any number of
                                     white spaces followed by '#'.
                                     A line that only contains white spaces is an argument.
                                     A comment starts by a '#' not following a '\'. The "\#"
                                     sequence is read as '#'.
    --args-file-charset <arg>        Optional name of the args file charset.
                                     It may be used if args file encoding is not the OS default file
                                     encoding.
    --attribute <arg>                Optional name(s) of attribute column(s) to compare.
                                     If omitted, all attribute columns are compared.
                                     If mapping is used, use new name(s).
    --auto-size-columns              Auto size columns. This may take longer time.
    --auto-size-rows                 Auto size rows. This may take longer time.
    --cell-diff-columns              Add a column for each key or data column describing differences
                                     of cells.
    --changed-mark <arg>             Optional mark for changed cells (default: "<C>").
    --charset <arg>                  Optional name of the charset for all CSV files (default:
                                     platform default charset).
                                     It can be overridden with --charset1 or --charset2 for input
                                     files.
    --charset1 <arg>                 Optional name of the charset for first input CSV file (default:
                                     value of --charset or platform default charset).
    --charset2 <arg>                 Optional name of the charset for second input CSV file
                                     (default: value of --charset or platform default charset).
    --colors                         Use colors (default with output formats that support colors).
    --diff-mark <arg>                Optional mark for diff columns (default: "Diff").
    --file1 <arg>                    Mandatory name of the first input file.
    --file1-mark <arg>               Optional mark for file1 columns (default: "1").
                                     Related to show-original-names option.
    --file2 <arg>                    Mandatory name of the second input file.
    --file2-mark <arg>               Optional mark for file2 columns (default: "2").
                                     Related to show-original-names option.
    --fix-headers                    If headers contain errors (duplicate or empty names), fix them.
                                     A renaming is applied, that can lead to unexpected results.
 -h,--help                           Prints this help and exits.
    --help-width <arg>               Optional help width (default: 74).
    --ignore-duplicates              If there are duplicates, the first row with a key is compared,
                                     and remaining rows with the same key are ignored.
    --key <arg>                      Mandatory name(s) of key column(s).
                                     If mapping is used, use new name(s).
    --line-diff-column               Add a column describing differences of lines (default).
    --map1 <arg>                     Optional mapping of column names (keys and attributes) of first
                                     file.
                                     Each mapping has the form <old name>::<new name>.
    --map2 <arg>                     Optional mapping of column names (keys and attributes) of
                                     second file.
                                     Each mapping has the form <old name>::<new name>.
    --marks                          Use marks (default with output formats that don't support
                                     colors).
    --max-rows <arg>                 Optional maximum number of rows per sheet (default: -1).
                                     A negative value means no maximum, or the format maximum.
                                     The number of generated sheets depends on that number and on
                                     the total number of rows.
                                     A very small positive number (0, 1, 2, ...) is irrelevant and
                                     will produce errors.
    --no-added-or-removed-marks      Do not output added or removed marks.
    --no-cell-diff-columns           Do not add a column for each key or data column describing
                                     differences of cells (default).
    --no-colors                      Do not use colors (default with output formats that don't
                                     support colors).
    --no-line-diff-column            Do not add a column describing differences of lines.
    --no-marks                       Do not use marks (default with output formats that support
                                     colors).
    --no-strict-csv                  If enabled, CSV files are NOT strictly parsed. Some invalid
                                     cells are accepted (default).
    --no-unchanged-lines             Do not output unchanged lines.
    --no-vulnerability-protections   Disable vulnerability protections such as detection of Zip
                                     bombs.
                                     This should be used with trusted sources.
    --output <arg>                   Mandatory name of the output file. It must have a supported
                                     extension.
    --pattern1 <arg>                 Optional pattern of the sheet(s) in the first input file.
    --pattern2 <arg>                 Optional pattern of the sheet(s) in the second input file.
    --removed-mark <arg>             Optional mark for removed cells (default: "<R>").
    --save-synthesis                 Save synthesis in output file, in a dedicated sheet.
    --separator <arg>                Optional char separator for all CSV files (default: ';').
                                     It can be overridden with --separator1 and --separator2 for
                                     input files.
    --separator1 <arg>               Optional char separator for first input CSV file (default:
                                     value of --separator or ';').
    --separator2 <arg>               Optional char separator for second input CSV file (default:
                                     value of --separator or ';').
    --sheet <arg>                    Optional name of the delta sheet in the output file. (default:
                                     "Delta").
    --sheet1 <arg>                   Optional name of the sheet in the first input file.
    --sheet2 <arg>                   Optional name of the sheet in the second input file.
    --show-change-details            If enabled, show value 1 (with removed mark or color) and value
                                     2 (with added mark or color).
                                     Otherwise, show value 2 (with changed mark or color).
    --show-original-names            If enabled, adds names of original columns in output columns.
                                     This is useful when name mapping is done.
    --sort-lines                     Sort lines using keys. Order of key columns declaration
                                     matters.
    --split-comparisons              If enabled, 2 columns are created for each pair of compared
                                     input columns, one for each input file.
    --strict-csv                     If enabled, CSV files are strictly parsed and checked for
                                     legality of '"' in cells.
    --synthesis                      Print a synthesis of differences on terminal.
    --unchanged-lines                Output unchanged lines (default).
    --unchanged-mark <arg>           Optional mark for unchanged cells (default: "").
 -v,--version                        Prints version and exits.
    --verbose                        Print progress messages.
    --vulnerability-protections      Enable vulnerability protections such as detection of Zip bombs
                                     (default).
    --zip32                          If enabled, use ZIP32 to generate Excel files.
    --zip64                          If enabled, use ZIP64 to generate Excel files (default).
KNOWN LIMITATIONS
All CSV files (input and output) must use the same charset and separator.
When mixing input file formats with CSV, if a key column contains numbers, comparison will fail.
Ods handling is experimental. Ods output does not support coloring.
```