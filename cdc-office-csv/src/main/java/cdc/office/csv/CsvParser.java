package cdc.office.csv;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.txt.LinesHandler;
import cdc.io.txt.LinesParser;
import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.TableHandler;
import cdc.office.tables.TableSection;
import cdc.office.tables.TablesHandler;
import cdc.util.function.Evaluation;
import cdc.util.lang.Checks;
import cdc.util.lang.InvalidStateException;

/**
 * Parsing of csv files. Special handling of text is done. However, one must not
 * use '"' as a field separator. One must indicate whether the file has a header
 * or not (this information is not automatically elaborated).
 *
 * @author Damien Carbonne
 */
public final class CsvParser {
    private static final Logger LOGGER = LogManager.getLogger(CsvParser.class);
    private char separator;
    private final Set<Hint> hints;

    public enum Hint {
        /** If enabled, traces are generated. */
        VERBOSE,
        /** If enabled, rows are counted if possible. */
        COUNT_ROWS,
        /** If enabled, rows are not generated. */
        VOID_HANDLER,
        /** If enabled, strict parsing of " is done. */
        STRICT_QUOTES
    }

    private CsvParser(Builder builder) {
        this.separator = builder.separator;
        this.hints = Set.copyOf(builder.hints);
    }

    /**
     * Creates a parser with ';' separator.
     *
     * @deprecated Use {@link Builder}.
     */
    @Deprecated(since = "2024-05-18", forRemoval = true)
    public CsvParser() {
        this(';');
    }

    /**
     * Creates a parser with a specified separator.
     *
     * @param separator the separator to use.
     * @deprecated Use {@link Builder}.
     */
    @Deprecated(since = "2024-05-18", forRemoval = true)
    public CsvParser(char separator) {
        this.separator = separator;
        this.hints = EnumSet.noneOf(Hint.class);
    }

    /**
     * Sets the used separator.
     *
     * @param separator the separator to use.
     * @return This parser.
     * @deprecated Use {@link Builder}.
     */
    @Deprecated(since = "2024-05-18", forRemoval = true)
    public CsvParser setSeparator(char separator) {
        this.separator = separator;
        return this;
    }

    /**
     * @param countRows If true, count rows.
     * @return This parser.
     * @deprecated Use {@link Builder}.
     */
    @Deprecated(since = "2024-05-18", forRemoval = true)
    public CsvParser countRows(boolean countRows) {
        if (countRows) {
            this.hints.add(Hint.COUNT_ROWS);
        } else {
            this.hints.remove(Hint.COUNT_ROWS);
        }
        return this;
    }

    /**
     * Sets the verbosity level of the parser.
     *
     * @param verbose indicates whether the parsing must be verbose or not.
     * @deprecated Use {@link Builder}.
     */
    @Deprecated(since = "2024-05-18", forRemoval = true)
    public void setVerbose(boolean verbose) {
        if (verbose) {
            this.hints.add(Hint.VERBOSE);
        } else {
            this.hints.remove(Hint.VERBOSE);
        }
    }

    private void traceBegin(Object object) {
        if (hints.contains(Hint.VERBOSE)) {
            LOGGER.info("Load: '{}' ... ", object);
        }
    }

    private void traceEnd() {
        if (hints.contains(Hint.VERBOSE)) {
            LOGGER.info("Done");
        }
    }

    private LinesHandler createLinesHandler(TableHandler handler,
                                            String systemId,
                                            int headers,
                                            int numberOfRows) {
        if (hints.contains(Hint.VOID_HANDLER)) {
            return new VoidHandler(handler,
                                   systemId,
                                   separator,
                                   hints.contains(Hint.STRICT_QUOTES),
                                   headers,
                                   numberOfRows);
        } else {
            return new Handler(handler,
                               systemId,
                               separator,
                               hints.contains(Hint.STRICT_QUOTES),
                               headers,
                               numberOfRows);
        }
    }

    /**
     * Parses a Reader.
     * <p>
     * If {@code reader} supports mark, the number of lines is computed.
     *
     * @param reader The reader to parse.
     * @param handler The table handler to use.
     * @param headers The number of header lines.
     * @throws IOException When an IO exception occurs.
     */
    public void parse(Reader reader,
                      TableHandler handler,
                      int headers) throws IOException {
        final int numberOfRows;
        if (hints.contains(Hint.COUNT_ROWS)) {
            if (reader.markSupported()) {
                reader.mark(Integer.MAX_VALUE);
                numberOfRows = CsvUtils.getNumberOfCsvRows(reader, separator);
                reader.reset();
            } else {
                LOGGER.warn("Cannot count rows, reader does not support marks.");
                numberOfRows = -1;
            }
        } else {
            numberOfRows = -1;
        }
        LinesParser.parse(reader,
                          createLinesHandler(handler, "?", headers, numberOfRows));
    }

    /**
     * Parses an InputStream.
     *
     * @param in The input stream.
     * @param systemId The system id.
     * @param charset The charset to use.
     * @param handler The table handler to use.
     * @param headers The number of header lines.
     * @throws IOException When an IO exception occurs.
     */
    public void parse(InputStream in,
                      String systemId,
                      Charset charset,
                      TableHandler handler,
                      int headers) throws IOException {
        final int numberOfRows;
        if (hints.contains(Hint.COUNT_ROWS)) {
            if (in.markSupported()) {
                in.mark(Integer.MAX_VALUE);
                numberOfRows = CsvUtils.getNumberOfCsvRows(in, systemId, charset, separator);
                in.reset();
            } else {
                LOGGER.warn("Cannot count rows, stream does not support marks.");
                numberOfRows = -1;
            }
        } else {
            numberOfRows = -1;
        }
        LinesParser.parse(in,
                          systemId,
                          charset,
                          createLinesHandler(handler, systemId, headers, numberOfRows));
    }

    /**
     * Parses an InputStream using default charset.
     *
     * @param in The input stream.
     * @param systemId The system id.
     * @param handler The table handler to use.
     * @param headers The number of header lines.
     * @throws IOException When an IO exception occurs.
     */
    public void parse(InputStream in,
                      String systemId,
                      TableHandler handler,
                      int headers) throws IOException {
        parse(in,
              systemId,
              Charset.defaultCharset(),
              handler,
              headers);
    }

    /**
     * Parses a file.
     *
     * @param file The input file.
     * @param charset The charset to use.
     * @param handler The table handler to use.
     * @param headers The number of header lines.
     * @throws IOException When an IO exception occurs.
     */
    public void parse(File file,
                      Charset charset,
                      TableHandler handler,
                      int headers) throws IOException {
        traceBegin(file);
        final int numberORows;
        if (hints.contains(Hint.COUNT_ROWS)) {
            numberORows = CsvUtils.getNumberOfCsvRows(file, charset, separator);
        } else {
            numberORows = -1;
        }
        TablesHandler.processBeginTables(handler, file.getPath());
        LinesParser.parse(file,
                          charset,
                          createLinesHandler(handler, file.getName(), headers, numberORows));
        TablesHandler.processEndTables(handler, file.getPath());
        traceEnd();
    }

    /**
     * Parses a file using default charset.
     *
     * @param file The input file.
     * @param handler The table handler to use.
     * @param headers The number of header lines.
     * @throws IOException When an IO exception occurs.
     */
    public void parse(File file,
                      TableHandler handler,
                      int headers) throws IOException {
        parse(file, null, handler, headers);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private char separator = ';';
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

        private Builder() {
        }

        public Builder separator(char separator) {
            this.separator = separator;
            return this;
        }

        public Builder hint(Hint hint) {
            this.hints.add(hint);
            return this;
        }

        public Builder hint(Hint hint,
                            boolean enabled) {
            if (enabled) {
                this.hints.add(hint);
            } else {
                this.hints.remove(hint);
            }
            return this;
        }

        public Builder hints(Hint... hints) {
            for (final Hint hint : hints) {
                this.hints.add(hint);
            }
            return this;
        }

        public Builder hints(Collection<Hint> hints) {
            for (final Hint hint : hints) {
                this.hints.add(hint);
            }
            return this;
        }

        public CsvParser build() {
            return new CsvParser(this);
        }

    }

    private abstract static class BaseHandler implements LinesHandler {
        private final TableHandler tableHandler;
        private final String systemId;
        private final char separator;
        private final boolean strictQuotes;
        private final int headers;
        private final int numberOfRows;
        private Status currentStatus = Status.LINE_OUT;
        private final RowLocation.Builder location = RowLocation.builder();
        private Evaluation evaluation = Evaluation.CONTINUE;
        private int escStartLine = -1;
        private int escStartCol = -1;
        private String escStartInput = null;

        private enum Status {
            /** Currently no row is processed */
            LINE_OUT,
            /** Currently, a standard cell (does not start with '"') is processed. */
            IN_STD_CELL,
            /** Currently, an escaped cell (starting with '"') is processed. */
            IN_ESC_CELL,
            /** Currently, an escaped cell parsed as a standard cell is processed. */
            IN_ESC_STD_CELL,
            /**
             * A quote ('"') is processed while in an escaped cell.
             * It should be followed by another '"', the separator, or the end of line.
             */
            QUOTE_IN_ESC_CELL,
            /** A cell has just been left. */
            CELL_OUT
        }

        BaseHandler(TableHandler handler,
                    String systemId,
                    char separator,
                    boolean strictQuotes,
                    int headers,
                    int numberOfRows) {
            Checks.isNotNull(handler, "handler");
            Checks.isTrue(headers >= 0, "Invalid headers");
            this.tableHandler = handler;
            this.systemId = systemId;
            this.separator = separator;
            this.strictQuotes = strictQuotes;
            this.headers = headers;
            this.numberOfRows = numberOfRows;
        }

        private String format(int lineNumber,
                              int charIndex) {
            return systemId + " [" + lineNumber + ":" + (charIndex + 1) + "]";
        }

        private String format(int lineNumber,
                              int charIndex,
                              String line) {
            return format(lineNumber, charIndex) + " '" + line + "'";
        }

        @Override
        public final void processBegin() {
            clearRow();
            tableHandler.processBeginTable(null, numberOfRows);
        }

        @Override
        public void processEnd() {
            if (currentStatus == Status.CELL_OUT || currentStatus == Status.LINE_OUT) {
                // Ignore
            } else if (strictQuotes) {
                // IN_STD_CELL, IN_ESC_CELL, IN_ESC_STD_CELL, QUOTE_IN_ESC_CELL
                throw new InvalidStateException("Reached end of input without closing the escaped cell started at "
                        + format(escStartLine, escStartCol, escStartInput));
            } else {
                addValue();
                flushLineIfAny();
            }
            tableHandler.processEndTable(null);
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (currentStatus == Status.IN_ESC_CELL) {
                appendNewLine();
            }
            for (int i = 0; i < line.length(); i++) {
                // current character
                final char c = line.charAt(i);
                if (c == '"') {
                    // Found the escape character
                    switch (currentStatus) {
                    case LINE_OUT, CELL_OUT -> {
                        // Start an escaped cell
                        // Track its location
                        setStatus(Status.IN_ESC_CELL);
                        escStartLine = number;
                        escStartCol = i;
                        escStartInput = line;
                    }

                    case IN_STD_CELL -> {
                        if (strictQuotes) {
                            throw new InvalidStateException("Found '\"' in standard cell at " + format(number, i, line));
                        } else {
                            // found '"' in standard cell
                            // This is not legal, but we accept it
                            // This means that the first character of the cell was not a '"'
                            if (LOGGER.isWarnEnabled()) {
                                LOGGER.warn("Found '\"' in non escaped cell at {}", format(number, i, line));
                            }
                            appendCharToValue('"');
                        }
                    }

                    case IN_ESC_STD_CELL -> {
                        // found '"' in escaped cell back to standard processing
                        // This is not legal, but we accept it
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Found '\"' in escaped-like cell at {}", format(escStartLine, escStartCol, escStartInput));
                        }
                        appendCharToValue('"');
                    }

                    case IN_ESC_CELL -> setStatus(Status.QUOTE_IN_ESC_CELL);
                    // Found '"' in escaped cell.
                    // It is either the end of the escaped cell, or the first of a pair of consecutive '"'
                    // This char should be followed by another '"' or by the separator

                    case QUOTE_IN_ESC_CELL -> {
                        // Found 2 consecutive '"' in an escaped cell
                        appendCharToValue('"');
                        setStatus(Status.IN_ESC_CELL);
                    }
                    }
                } else if (c == separator) {
                    if (currentStatus == Status.IN_ESC_CELL) {
                        appendCharToValue(c);
                    } else {
                        // QUOTE_IN_ESC_CELL: this was the closing '"'
                        // IN_STD_CELL: a standard cell, possibly empty is closed
                        // LINE_OUT: line starts with the separator, its first cell is empty
                        // CELL_OUT: we just left a cell, so current cell is empty
                        addValue();
                        // now CELL_OUT
                    }
                } else {
                    switch (currentStatus) {
                    case LINE_OUT, CELL_OUT -> {
                        // Start a standard cell
                        appendCharToValue(c);
                        setStatus(Status.IN_STD_CELL);
                    }

                    case IN_STD_CELL, IN_ESC_CELL, IN_ESC_STD_CELL ->
                            // Don't change status
                            appendCharToValue(c);

                    case QUOTE_IN_ESC_CELL -> {
                        if (strictQuotes) {
                            // A normal character (neither '"' nor the separator) is found after a '"' has been found
                            // in an escaped cell
                            // This is an error
                            throw new InvalidStateException("Expected '\"' or '" + separator + "' after '\"', but found '" + c
                                    + "' at " + format(number, i, line)
                                    + ", in escaped cell started at " + format(escStartLine, escStartCol, escStartInput));
                        } else {
                            // This is not legal, but we accept it
                            if (LOGGER.isWarnEnabled()) {
                                LOGGER.warn("Found '{}' after '\"' in escaped cell started at {}",
                                            c,
                                            format(escStartLine, escStartCol, escStartInput));
                            }
                            appendCharToValue(c);
                            setStatus(Status.IN_ESC_STD_CELL);
                        }
                    }
                    }
                }
            }

            // End of line processing
            switch (currentStatus) {
            case IN_STD_CELL, IN_ESC_STD_CELL, QUOTE_IN_ESC_CELL, CELL_OUT -> {
                addValue(); // Possibly add an empty one
                // Now CELL_OUT
                flushLineIfAny();
                // Now LINE_OUT
            }
            case IN_ESC_CELL -> {
                // Ignore
            }
            case LINE_OUT -> flushLineIfAny();
            }
            return evaluation;
        }

        private void setStatus(Status status) {
            currentStatus = status;
        }

        /**
         * Appends a character to current cell.
         *
         * @param c The character.
         */
        abstract void appendCharToValue(char c);

        /**
         * Appends New Line to current (escaped) cell.
         */
        private void appendNewLine() {
            appendCharToValue('\n');
        }

        /**
         * Adds content of current cell to current row and resets current cell.
         */
        private void addValue() {
            addValueToRow();
            setStatus(Status.CELL_OUT);
            escStartCol = -1;
            escStartLine = -1;
            escStartInput = null;
        }

        abstract void addValueToRow();

        abstract void clearRow();

        abstract Row buildRow();

        /**
         * Push current row, then resets current row and current cell.
         */
        private void flushLineIfAny() {
            location.incrementNumbers(headers);
            if (location.getSection() == TableSection.HEADER) {
                evaluation = tableHandler.processHeader(buildRow(),
                                                        location.build());
            } else {
                evaluation = tableHandler.processData(buildRow(),
                                                      location.build());
            }

            clearRow();
            setStatus(Status.LINE_OUT);
            escStartCol = -1;
            escStartLine = -1;
            escStartInput = null;
        }
    }

    /**
     * Parsing of input lines, one after the other.
     *
     * @author D. Carbonne
     *
     */
    private static class Handler extends BaseHandler {
        /** The current cell. */
        private final StringBuilder currentValue = new StringBuilder();
        /** The current row. */
        private final Row.Builder row = Row.builder();

        Handler(TableHandler handler,
                String systemId,
                char separator,
                boolean strictQuotes,
                int headers,
                int numberOfRows) {
            super(handler, systemId, separator, strictQuotes, headers, numberOfRows);
        }

        /**
         * Appends a character to current cell.
         *
         * @param c The character.
         */
        @Override
        void appendCharToValue(char c) {
            currentValue.append(c);
        }

        @Override
        void addValueToRow() {
            row.addValue(currentValue.toString());
            currentValue.setLength(0);
        }

        @Override
        void clearRow() {
            row.clear();
            currentValue.setLength(0);
        }

        @Override
        Row buildRow() {
            return row.build();
        }
    }

    private static class VoidHandler extends BaseHandler {
        VoidHandler(TableHandler handler,
                    String systemId,
                    char separator,
                    boolean strictQuotes,
                    int headers,
                    int numberOfRows) {
            super(handler, systemId, separator, strictQuotes, headers, numberOfRows);
        }

        @Override
        void appendCharToValue(char c) {
            // Ignore
        }

        @Override
        void addValueToRow() {
            // Ignore
        }

        @Override
        void clearRow() {
            // Ignore
        }

        @Override
        Row buildRow() {
            return null;
        }
    }
}