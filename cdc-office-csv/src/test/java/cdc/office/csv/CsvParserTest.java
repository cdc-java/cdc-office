package cdc.office.csv;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.TableHandler;
import cdc.util.function.Evaluation;
import cdc.util.lang.InvalidStateException;

class CsvParserTest {
    protected static final Logger LOGGER = LogManager.getLogger(CsvParserTest.class);
    private static final char SEPARATOR = ';';
    private final List<Row> rows = new ArrayList<>();

    private final File filePlatform = new File("target", getClass().getSimpleName() + "-platform.txt");

    CsvParserTest() throws IOException {
        rows.add(Row.builder("V1", "V2").build());
        rows.add(Row.builder("Aaaa", "ààà").build());
        rows.add(Row.builder("Bbbb", "ÖöÏï").build());

        // Generate a Platform file
        try (PrintStream out = new PrintStream(filePlatform)) {
            for (final Row row : rows) {
                boolean first = true;
                for (final String value : row.getValues()) {
                    if (first) {
                        first = false;
                    } else {
                        out.print(SEPARATOR);
                    }
                    out.print(value);
                }
                out.println();
            }
            out.close();
        }
    }

    private static void checkOneRow(List<String> expected,
                                    String input,
                                    char separator,
                                    CsvParser.Hint... hints) throws IOException {
        final CsvParser parser =
                CsvParser.builder()
                         .separator(separator)
                         .hints(hints)
                         .build();
        final Handler handler = new Handler();
        try (final InputStream is = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))) {
            parser.parse(is, "???", handler, 0);
        }
        assertEquals(1, handler.rows.size());
        assertEquals(expected, handler.rows.get(0).getValues());
    }

    private static void checkNRows(List<List<String>> expected,
                                   String input,
                                   char separator,
                                   CsvParser.Hint... hints) throws IOException {
        final CsvParser parser =
                CsvParser.builder()
                         .separator(separator)
                         .hints(hints)
                         .build();
        final Handler handler = new Handler();
        try (final InputStream is = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))) {
            parser.parse(is, "???", handler, 0);
        }
        assertEquals(expected.size(), handler.rows.size());
        for (int index = 0; index < expected.size(); index++) {
            assertEquals(expected.get(index), handler.rows.get(index).getValues());
        }
    }

    private static void checkInput(String input,
                                   CsvParser.Hint... hints) throws IOException {
        final Handler handler = new Handler();
        final CsvParser parser =
                CsvParser.builder()
                         .separator(SEPARATOR)
                         .hints(hints)
                         .build();
        try (final InputStream is = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))) {
            parser.parse(is, "???", handler, 0);
        }
    }

    private static void checkValidInput(String input,
                                        int... cellsPerLine) throws IOException {
        final Handler handler = new Handler();
        final CsvParser parser =
                CsvParser.builder()
                         .separator(SEPARATOR)
                         .build();
        try (final InputStream is = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))) {
            parser.parse(is, "???", handler, 0);
        }
        assertSame(cellsPerLine.length, handler.rows.size());
        for (int i = 0; i < cellsPerLine.length; i++) {
            assertSame(cellsPerLine[i], handler.rows.get(i).size());
        }
    }

    private static void checkInvalidInput(String input,
                                          CsvParser.Hint... hints) {
        assertThrows(InvalidStateException.class,
                     () -> {
                         try {
                             checkInput(input, hints);
                         } catch (final InvalidStateException e) {
                             LOGGER.info(e.getMessage());
                             throw e;
                         }
                     });
    }

    private static final class Handler implements TableHandler {
        public List<Row> rows = new ArrayList<>();
        public int headers = 0;
        public int datas = 0;

        public Handler() {
            super();
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            // Ignore
            LOGGER.debug("processBeginTable({}, {})", name, numberOfRows);
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            LOGGER.debug("processHeader({}, {})", header, location);
            rows.add(header);
            headers++;
            return Evaluation.CONTINUE;
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            LOGGER.debug("processData({}, {})", data, location);
            rows.add(data);
            datas++;
            return Evaluation.CONTINUE;
        }

        @Override
        public void processEndTable(String name) {
            // Ignore
            LOGGER.debug("processEndTable({})", (Object) null);
        }

    }

    private void testParseFile(File file,
                               Charset charset) throws Exception {
        LOGGER.debug("testParseFile({}, {})", file, charset);

        final Handler handler = new Handler();
        final CsvParser parser =
                CsvParser.builder()
                         .separator(SEPARATOR)
                         .build();
        parser.parse(file, charset, handler, 1);
        assertEquals(rows, handler.rows);
        assertEquals(1, handler.headers);
        assertEquals(2, handler.datas);
    }

    @Test
    void testParseFile() throws Exception {
        testParseFile(filePlatform, Charset.defaultCharset());
        // testParseFile(fileUtf8, "UTF-8");
        // testParseFile(fileUtf16, "UTF-16");
    }

    @Test
    void checkValidInput() throws IOException {
        checkValidInput("");
        checkValidInput("A", 1);
        checkValidInput(";", 2);
        checkValidInput("A;", 2);
        checkValidInput("A;B", 2);
        checkValidInput(";;", 3);
        checkValidInput("\n", 0);
        checkValidInput("A\n", 1);
        checkValidInput("A\nA", 1, 1);
        checkValidInput("\"\"", 1);
        checkValidInput("\"\"\"\"", 1);
        checkValidInput("A;\"\"\"\";C", 3);

        checkValidInput("s\"", 1); // Invalid in strict mode
        checkValidInput("s\"\"\"a", 1); // Invalid in strict mode
    }

    @Test
    void checkInvalidInput() {
        // checkInvalidInput("\"");
        checkInvalidInput("\"", CsvParser.Hint.STRICT_QUOTES);
        checkInvalidInput("s\"", CsvParser.Hint.STRICT_QUOTES);
        // checkInvalidInput("\"s");
        checkInvalidInput("\"s", CsvParser.Hint.STRICT_QUOTES);
        // checkInvalidInput("\"\"s");
        checkInvalidInput("\"\"s", CsvParser.Hint.STRICT_QUOTES);
        // checkInvalidInput("\"\"\"s");
        checkInvalidInput("\"\"\"s", CsvParser.Hint.STRICT_QUOTES);
        checkInvalidInput("s\"", CsvParser.Hint.STRICT_QUOTES);
    }

    @Test
    void testOneRowStrictQuotes() throws IOException {
        final char separator = ';';
        final CsvParser.Hint[] hints = { CsvParser.Hint.STRICT_QUOTES };
        checkOneRow(List.of("A"), "A", separator, hints);
        checkOneRow(List.of("A"), "\"A\"", separator, hints);
        checkOneRow(List.of("A", "B"), "A;B", separator, hints);
        checkOneRow(List.of("\"A\""), "\"\"\"A\"\"\"", separator, hints);
        checkOneRow(List.of("A", ""), "A;", separator, hints);
        checkOneRow(List.of("A;"), "\"A;\"", separator, hints);
        checkOneRow(List.of("A\nA"), "\"A\nA\"", separator, hints);
        checkOneRow(List.of("A\nA", "B"), "\"A\nA\";B", separator, hints);
        assertThrows(InvalidStateException.class,
                     () -> {
                         checkOneRow(null, " \"A;", separator, hints);
                     });
    }

    @Test
    void testNRowsStrictQuotes() throws IOException {
        final char separator = ';';
        final CsvParser.Hint[] hints = { CsvParser.Hint.STRICT_QUOTES };
        checkNRows(List.of(List.of("A"), List.of("A")), "A\nA", separator, hints);
        checkNRows(List.of(List.of("A\nA", "B"), List.of("C")), "\"A\nA\";B\nC", separator, hints);
        checkNRows(List.of(List.of("A;\nA;", "B"), List.of("C")), "\"A;\nA;\";B\nC", separator, hints);
    }

    @Test
    void testOneRowRelaxedQuotes() throws IOException {
        final char separator = ';';
        final CsvParser.Hint[] hints = {};
        checkOneRow(List.of("A"), "A", separator, hints);
        checkOneRow(List.of("A"), "\"A\"", separator, hints);
        checkOneRow(List.of("A", "B"), "A;B", separator, hints);
        checkOneRow(List.of("\"A\""), "\"\"\"A\"\"\"", separator, hints);
        checkOneRow(List.of("A", ""), "A;", separator, hints);
        checkOneRow(List.of("A;"), "\"A;\"", separator, hints);
        checkOneRow(List.of(" \"A"), " \"A", separator, hints);
        checkOneRow(List.of(" \"A", ""), " \"A;", separator, hints);
        checkOneRow(List.of(" \"C\""), " \"C\"", separator, hints);
        checkOneRow(List.of(" \"C\"", ""), " \"C\";", separator, hints);
        checkOneRow(List.of(" \"D\" "), " \"D\" ", separator, hints);
        checkOneRow(List.of(" \"D\" ", ""), " \"D\" ;", separator, hints);
        checkOneRow(List.of(" \"F\" F "), " \"F\" F ", separator, hints);
        checkOneRow(List.of(" \"F\" F ", ""), " \"F\" F ;", separator, hints);
        checkOneRow(List.of(" \"H\" \"H\""), " \"H\" \"H\"", separator, hints);
        checkOneRow(List.of(" \"H\" \"H\"", ""), " \"H\" \"H\";", separator, hints);
        checkOneRow(List.of(" \"K\"\"K\" "), " \"K\"\"K\" ", separator, hints);
        checkOneRow(List.of(" \"K\"\"K\" ", ""), " \"K\"\"K\" ;", separator, hints);

        checkOneRow(List.of("B "), "\"B\" ", separator, hints);
        checkOneRow(List.of("B ", ""), "\"B\" ;", separator, hints);
        checkOneRow(List.of("E E"), "\"E\" E", separator, hints);
        checkOneRow(List.of("E E", ""), "\"E\" E;", separator, hints);
        checkOneRow(List.of("G \"G\""), "\"G\" \"G\"", separator, hints);
        checkOneRow(List.of("G \"G\"", ""), "\"G\" \"G\";", separator, hints);
        checkOneRow(List.of("II\""), "\"I\"I\"", separator, hints);
        checkOneRow(List.of("II\"", ""), "\"I\"I\";", separator, hints);
        checkOneRow(List.of("L\""), "\"L\"\"", separator, hints);
    }
}