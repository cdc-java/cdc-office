package cdc.office.ss.excel;

public enum PoiStreaming {
    /** No streaming. */
    IN_MEMORY,
    /** Streaming with ZIP32. */
    STREAMING_ZIP32,
    /** Streaming with ZIP64. */
    STREAMING_ZIP64;

    public boolean isStreaming() {
        return this == STREAMING_ZIP32 || this == STREAMING_ZIP64;
    }
}