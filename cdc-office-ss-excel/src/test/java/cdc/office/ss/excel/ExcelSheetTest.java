package cdc.office.ss.excel;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.junit.jupiter.api.Test;

import cdc.office.ss.SheetParserFactory;
import cdc.office.ss.SheetTestSupport;
import cdc.office.ss.WorkbookWriterFeatures;

/**
 * Test generation and parsing of sheets using different formats and options.
 *
 * @author Damien Carbonne
 *
 */
class ExcelSheetTest extends SheetTestSupport {
    @Test
    void testXlsDefault() throws Exception {
        check("-default.xls",
              factory -> {
                  // Ignore
              },
              factory -> {
                  // Ignore
              });
    }

    @Test
    void testXlsxDefault() throws Exception {
        check("-default.xlsx",
              factory -> {
                  // Ignore
              },
              factory -> {
                  // Ignore
              });
    }

    @Test
    void testXlsmDefault() throws Exception {
        check("-default.xlsm",
              factory -> {
                  // Ignore
              },
              factory -> {
                  // Ignore
              });
    }

    @Test
    void testXlsxStandard() throws Exception {
        check("-standard.xlsx",
              factory -> {
                  // Ignore
              },
              factory -> {
                  factory.setEnabled(SheetParserFactory.Feature.POI_STANDARD, true);
              });
    }

    @Test
    void testXlsmStandard() throws Exception {
        check("-standard.xlsm",
              factory -> {
                  // Ignore
              },
              factory -> {
                  factory.setEnabled(SheetParserFactory.Feature.POI_STANDARD, true);
              });
    }

    @Test
    void testXlsxStreaming() throws Exception {
        check("-streaming.xlsx",
              factory -> {
                  // Ignore
              },
              factory -> {
                  factory.setEnabled(SheetParserFactory.Feature.POI_STREAMING, true);
              });
    }

    @Test
    void testXlsmStreaming() throws Exception {
        check("-streaming.xlsm",
              factory -> {
                  // Ignore
              },
              factory -> {
                  factory.setEnabled(SheetParserFactory.Feature.POI_STREAMING, true);
              });
    }

    @Test
    void testAutosize() throws Exception {
        final String cellContent = "XXX XXX XXXX XXX XXX XXX"
                + "\nXXX XXX XXXX XXX XXX XXX"
                + "\nXXX XXX XXXX XXX XXX XXX";

        final int[] sheetsA = { 1, 2, 3 };
        final int[] rowsA = { 1, 2, 10 };

        for (final int sheets : sheetsA) {
            for (final int rows : rowsA) {
                generate(new File("target/autosize-" + sheets + "-5-" + rows + ".xlsx"),
                         sheets,
                         5,
                         rows,
                         cellContent,
                         factory -> {
                             // Ignore
                         },
                         WorkbookWriterFeatures.STANDARD_BEST);
            }
        }
        assertTrue(true);
    }
}