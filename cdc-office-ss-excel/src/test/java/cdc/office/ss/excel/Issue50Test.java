package cdc.office.ss.excel;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.office.ss.CellAddressRange;
import cdc.office.ss.ContentValidation;
import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.tables.TableSection;

class Issue50Test {
    private static final Logger LOGGER = LogManager.getLogger(Issue50Test.class);

    private static void test(String filename,
                             int total) throws IOException {
        final File file = new File("target", filename);
        final WorkbookKind kind = WorkbookKind.from(file);

        LOGGER.info("generate {}", file);
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        try (final WorkbookWriter<?> w = factory.create(file)) {
            w.beginSheet("Sheet");
            w.beginRow(TableSection.HEADER);
            w.addCell("Title");
            final List<String> values = new ArrayList<>();

            int length = 0;
            int i = 0;
            while (length < total) {
                final String next = "Value" + i;
                final int remaining = total - length - (i == 0 ? 0 : 1);
                if (remaining >= next.length()) {
                    values.add(next);
                } else {
                    values.add(next.substring(next.length() - remaining));
                }
                length += values.get(values.size() - 1).length();
                if (i > 0) {
                    length++;
                }
                i++;
            }
            LOGGER.info("length: {}", length);

            final ContentValidation cv =
                    ContentValidation.builder()
                                     .help("Title", "Blah blah ...")
                                     .error("Title", "Error in ...")
                                     .errorReaction(ContentValidation.ErrorReaction.WARN)
                                     .allowsEmptyCell(true)
                                     .type(ContentValidation.Type.LIST)
                                     .operator(ContentValidation.Operator.NONE)
                                     .values(values)
                                     .addRange(new CellAddressRange(1, -1, 0, 0))
                                     .build();

            final int max = kind.getMaxValidationExplicitListLength();
            if (max > 0 && cv.getValuesLength() > max) {
                LOGGER.error("Too many validation explicit list characters: {}", cv.getValuesLength());
            }
            w.addContentValidation(cv);
        }
    }

    @Test
    void testXlsx() throws IOException {
        test("issue50-10.xlsx", 10);
        test("issue50-250.xlsx", 250);
        test("issue50-255.xlsx", 255);
        assertThrows(IllegalArgumentException.class,
                     () -> test("issue50-256.xlsx", 256));
    }

    @Test
    void testXls() throws IOException {
        test("issue50-10.xls", 10);
        test("issue50-250.xls", 250);
        test("issue50-255.xls", 255);
        assertThrows(IllegalArgumentException.class,
                     () -> test("issue50-256.xls", 256));
    }
}