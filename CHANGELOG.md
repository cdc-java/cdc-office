# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.60.1] - 2025-02-23
### Added
- Added `--charset1`, `--separator1` and `--charset2`, `--separator2` options to KSD.

### Changed
- Updated dependencies:
    - org.junit-5.12.0


## [0.60.0] - 2025-02-08
### Added
- Created `cdc.office.ss.excel.PoiStreaming`.


## [0.59.0] - 2025-01-11
### Added
- Added `WorkbookWriterFactory.Hint.ZIP32` that should be used with `WorkbookWriterFactory.Hint.POI_STREAMING`
  when files do not require ZIP64.
- Added `--zip32` and `--zip64` options to KSD.
 
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - org.apache.poi-5.4.0

### Deprecated
- Deprecated some `ExcelWorkbookWriter` constructors.


## [0.58.1] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-io-0.53.0
    - cdc-kernel-0.51.3
    - cdc-util-0.54.0
    - commons-io-2.18.0
    - org.apache.log4j-2.24.3
    - org.junit-5.11.4


## [0.58.0] - 2024-11-11
### Added
- Added URL variants to parse and load sheets.

### Changed
- Updated dependencies:
    - org.apache.log4j-2.24.1
    - org.junit-5.11.3


## [0.57.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-kernel-0.51.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - commons-io-2.17.0
    - org.junit-5.11.1


## [0.57.1] - 2024-07-22
### Fixed
- Return name or pattern in toString() of HeaderCell.


## [0.57.0] - 2024-07-21
### Added
- Added new method to `HeaderMapper`.
- Created `BasicHeaderMapper`. 
 
### Changed
- Improved detection of invalid headers when patterns are used.
- **Breaking:** `Header.getMatchingCell()` now returns an `Optional`.
- Updated dependencies:
    - org.apache.poi-5.3.0


## [0.56.0] - 2024-06-02
### Added
- Added `WorkbookKind.getMaxSheetName()`.
- Added indexing configuration to `WorkbookWriterHelper` and `KeyedTableDiffExporter`.

### Changed
- `KeyedTableDiffExporter` must now be constructed with a Builder.  
  It can be used by external tools such as ASD models comparator.

### Deprecated
- Deprecated construction methods of ̀ KeyedTableDiffExporter`. `Builder` must now be used.


## [0.55.1] - 2024-05-25
### Changed
- Supported parsing of more invalid CSV cases (bad escape). #60
- Updated dependencies:
    - commons-cli-1.8.0


## [0.55.0] - 2024-05-18
### Added
- Created `CsvParser.Builder`.

### Changed
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-kernel-0.51.1
    - cdc-util-0.52.1
    - commons-cli-1.7.0
    - commons-io-2.16.1
    - org.apache.log4j-2.23.1
- Updated maven plugins.
- Added `strict-csv` option to `KSD`. Now, KSD parses CSV files in non strict mode by default. #57

### Deprecated
- Deprecated `CsvParser` public construction and modification.  
  One must now use `CsvParser.Builder`.

### Fixed
- Fixed handling of large numbers in `KSD` when `--sort-lines` is enabled.  
  This fix comes with `cdc-util-0.52.1`. #59 


## [0.54.0] - 2024-03-10
### Changed
- Updated dependencies:
    - cdc-io-0.51.1
    - org.junit-5.10.2
- Improved error messages in `CsvParser`. Parsing is now stricter. #54

### Fixed
- Fixed IndexOutOfBoundsException in `Rows.filter()`. #55


## [0.53.0] - 2024-01-21
### Changed
- Displayed indices of null header columns in `KSD`. #52
- Added `--fix-headers` option to `KSD`. When enabled, invalid headers are fixed. #52
- Added `--ignore-duplicates` option to `KSD`. When enabled, rows with duplicate keys are ignored. #53


## [0.52.0] - 2024-01-01
### Added
- Added `WorkbookWriterFeatures.Feature.WRAP_TEXT`.
- Implemented `addCellComment(comment)` for ODS. #25
- Implemented `addCell(URI, label)` for ODS. #25

### Changed
- Text cells are no more wrapped.One must enable `WRAP_TEXT`.
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-kernel-0.51.0
    - cdc-util-0.52.0
    - odftoolkit-0.12.0
- Updated maven plugins.

### Removed
- Removed useless dependencies for HTML. In addition they added vulnerabilities.


## [0.51.0] - 2023-12-10
### Added
- Added new methods (`getNumberOfSheets`, ...) to `WorkbookWriter`.
- Created `WorkbookWriterHelper` that can help write several sheets when there are too many rows for one sheet. #51
- Added new options to `KSD`: #51
    - `--max-rows` to control the maximum number of rows per diff sheet.
    - `--pattern1` and `--pattern2` to load and merge several sheets in loaded workbooks.
- Created `TablesMerger` class. #51
- Added new methods to `SheetLoader` to load several sheets and merge them, collect sheet names. #51

### Changed
- Updated dependencies:
    - cdc-util-0.51.0
    - commons-io-2.15.1
- `KSD` now saves differences to several sheets. #51
- Added `exec()` and call `System.exit()` in `Anonymizer`,
  `MultiplyShiftHashSearcher`, `SeparatorConverter` and `SheetExtractor`.


## [0.50.0] - 2023-11-25
### Added
- Added `WorkbookKind.getMaxValidationExplicitListLength()`. #50
- Added methods to `ContentValidation` to check values length. #50

### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-kernel-0.50.0
    - cdc-tuples-1.4.0
    - cdc-util-0.50.0
    - org.apache.poi-5.2.5


## [0.31.0] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-kernel-0.23.2
    - cdc-tuples-1.3.0
    - cdc-util-0.33.2
    - commons-cli-1.6.0
    - commons-io-2.15.0
    - org.junit-5.10.1
- In `KSD`, when a column has no name, a better message is generated. #48

### Fixed
- Fixed handling of `AUTO_SIZE_ROWS` of last row. It was only working for last sheet. #49

### Removed
- Removed code that was deprecated more than 1 year ago.


## [0.30.2] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-kernel-0.23.1
    - cdc-util-0.33.1
    - commons-io-2.14.0
    - org.apache.poi-5.2.4
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.30.1] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-kernel-0.22.1
    - cdc-util-0.33.0


## [0.30.0] - 2023-06-11
### Removed
- Removed old deprecated code

### Changed
- Updated dependencies:
    - cdc-io-0.26.2
    - cdc-kernel-0.22.0
    - cdc-util-0.32.0
    - commons-io-2.13.0


## [0.29.4] - 2023-04-22
### Changed
- Updated dependencies:
    - cdc-kernel-0.21.4


## [0.29.3] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-kernel-0.21.3


## [0.29.2] - 2023-04-01
### Fixed
- Fixed `AUTO_SIZE_ROWS` for last row of last sheet. #47
- Fixed some pom dependencies

### Changed
- Updated dependencies:
    - cdc-io-0.26.0


## [0.29.1] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-kernel-0.21.2
    - cdc-util-0.31.0
    - htmlflow-4.0
    - org.apache.log4j-2.20.0
    
### Fixed
- Fixed bug with `--sort-lines` option in KSD.
  The bug was in `cdc-util` and was fixed with release 0.30.0. #46


## [0.29.0] - 2023-01-28
### Added
- Added new options to KSD:  
  `colors`,  
  `marks`, `no-marks`,  
  `show-original-names`,  
  `line-diff-column`, `no-line-diff-column`,  
  `cell-diff-columns`, `no-cell-diff-columns`,  
  `split-comparisons`. 
  `file1-mark`, `file2-mark`. #43
- Added new options to KSD:  
  `added-or-removed-marks`,  
  `vulnerability-protection`.

### Changed
- Renamed `line-mark-column` to `diff-mark`. #43
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-kernel-0.21.1
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.28.1] - 2023-01-02
### Added
- Added `KeydSheetDiff.Feature.AUTO_SIZE_ROWS`.


## [0.28.0] - 2023-01-02
### Added
- Added `WorkbookWriterFeatures.Feature.AUTO_SIZE_ROWS`.
- Added `--attribute` option to `ksd`. It can be used to select attribute columns to compare. #40
- Added `--map1` and `--map2` options to `ksd`. They can be used to rename columns. #41

### Changed
- Removed annoying warning and debug traces in Excel writers
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-kernel-0.21.0
    - cdc-util-0.28.2
    - odftoolkit-0.11.0


## [0.27.0] - 2022-11-12
### Added
- Added `WorkbookWriterFeatures.Feature.RICH_TEXT` to explicitly indicate that rich text should be supported. #39

### Fixed
- Reduced memory usage and improved performances with XLSX output format in `KeyedTableDiffExporter`.  
  Streaming workbook now supports rich text. #37
- Fixed performance regression introduced by #37 when compared to 0.26.0. #39

### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-kernel-0.20.7
    - cdc-util-0.28.1
- Modified `Header` and `HeaderMapper` to support patterns. Now a Header is composed of cells. #38
- Changed `addCells(List<String>)` to `addCells(List<? extends Object>)` in `WorkbookWriter`.

### Removed
- Removed  deprecated constructors (since 2022-05-19) of `Header`.


## [0.26.0] - 2022-10-16
### Changed
- Created `KeyedTableDiff.Builder`.
- Automatically remove empty trailing rows in `MemoryTableHandler()`.
  To keep them, use `MemoryTableHandler(false)`. #36

### Removed
- Removed public constructors of `KeyedTableDiff`. Use `KeyedTableDiff.Builder`.

### Fixed
- Ignore empty rows in `KeyedTableDiff`. #36
- Fixed `PoiSaxSheetParser`. It wrongly generated an initial empty row.


## [0.25.1] - 2022-10-15
### Changed
- `RowDiff` now accept more data in a row than declared in the corresponding header. #35

### Fixed
- Added detection of invalid sheet name or sheet index in POI SAX and POI Stream loading. #34
- Added a check in `ksd` to detect empty sheets and avoid throwing unexpected exception.


## [0.25.0] - 2022-10-02
### Added
- Created `ksd` documentation. #19
- Added a `SheetParserFactory.Feature.DISABLE_VULNERABILITY_PROTECTIONS` to disable vulnerability detection
  such as Zip bombs. #33
- Added a new option to `ksd` to disable disable vulnerability detection. #33

### Changed
- Improved error message in `Header`.
- Renamed `KeyedTableDiff.Synthesis.Action.UNCHANGED` to `SAME`. #32
- Updated dependencies:
    - org.apache.log4j-2.19.0
    - org.apache.poi-5.2.3
    - org.junit-5.9.1

### Fixed
- Fixed the `--no-added-or-removed-marks`option in `ksd`. #31


## [0.24.0] - 2022-08-24
### Added
- Added `WorkbookWriter.addEmptyRows(count)`.

### Changed
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-kernel-0.20.6
    - cdc-tuples-1.2.0
    - cdc-util-0.28.0
    - org.junit-5.9.0

### Fixed
- Improved computation of the size of cell comments. #29
- Do not generate warning message for non supported content validation in `CsvWorkbookWriter`.
- Added a `FULL_CHECK` option to `MultiplyShiftHashSearcher`.


## [0.23.1] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-kernel-0.20.5
    - cdc-util-0.27.0
    - com.j2html-1.6.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1

### Fixed
- Automatically limited size of error/help messages for data content validation. #26
- Better computation of the size of the comment box. Things can still be improved. #27


## [0.23.0] - 2022-06-18
### Added
- Added Data validation. At the moment it is only implement for Excel formats. #21
- Added `WorkbookWriterFeatures.Fature.CONTENT_VALIDATION`. #21

### Changed
- Improved error message when a key is missing in `KeyedSheetDiff`. #23
- Paths are now relative to args file location in `KeyedSheetDiff`. #22
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-kernel-0.20.4
    - cdc-util-0.26.0

### Fixed
- Excluded the invalid odf dependency in pom.xml. This exclusion should be remove in the future. cdc-java/cdc-deps#17
- Removed automatically created sheet when using odftoolkit. #24


## [0.22.0] - 2022-05-21
### Added    
- Numerical comparison of integer tail of strings in `KeyedTableDiffExporter`. #17
- Created `Header.Builder`. #16
- Added  `VERBOSE` option to `KeyedSheetDiff`. #18
- Added `SAVE_SYNTHESIS` option to `KeyedSheetDiff`. #20

### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-kernel-0.20.3
    - cdc-tuples-1.1.0
    - cdc-util-0.25.0
    - org.apache.poi-5.2.2

### Deprecated
- Deprecate `Header` constructors. #16


## [0.21.1] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-io-0.21.1
    - cdc-kernel-0.20.2
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2
    - org.apache.poi-5.2.1
- `Config` data is now retrieved from Manifest.


## [0.21.0] - 2022-02-13
### Added
- Added `CsvWriter` constructors using `OutputStream`.  #15
- Added creation of `WorkbookWriter` using an OutputStream. #15

### Changed
- Resources (`Writer`, `OutputStream`, `PrintStream`) that are passed to CsvWriter are not closed.  
  Some `CsvWriter` constructors have been deprecated. #15
- Updated dependencies
    - cdc-io-0.21.0
    - cdc-kernel-0.20.1
    - cdc-util-0.20.0

### Fixed
- Added support of '"' in `MultiplyShiftHashSearcher`.


## [0.20.0] - 2022-02-05
### Changed
- Upgraded to Java 11
- Updated dependencies
    - cdc-io-0.20.0
    - cdc-kernel-0.20.0
    - odftoolkit-0.10.0. Support of ODS is far from perfect. It was not before. #14
    - htmlflow-3.9


## [0.15.0] - 2022-01-28
### Added
- Created `VerboseTablesHandler`.
- Created `Issue12Test` to analyze issue #11. No change to CDC code was identified. #11

### Removed
- Removed deprecated methods `Row.getColumnsCount()`, `TableHandler.processBegin()`
  and `TableHandler.processEnd()`.

### Changed
- Changed API of HeaderMapper. Now it can handler mandatory and optional names.  
  The constructor is now private and a Builder has been added.  #13
- Updated maven plugins

### Fixed
- Fixed `PoiStreamSheetParser` and `PoiSaxSheetPrser` so that POI does not generate
  any warning message when a READ OPCPackage is closed.
  The following message:  
  *WARN org.apache.poi.openxml4j.opc.OPCPackage - The close() method is intended to SAVE a package. This package is open in READ ONLY mode, use the revert() method instead!*  
  should not happen anymore. #12


## [0.14.2] - 2022-01-15
### Added
- Added `WorkbookWriter.addCellAndComment()`.

### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - cdc-io-0.13.2
    - cdc-kernel-0.14.2
    - org.apache.log4j-2.17.1. #10
    - org.apache.poi-5.2.0 #10


## [0.14.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - cdc-io-0.13.1
    - cdc-kernel-0.14.1
    - fastods-0.8.1
    - odftoolkit-0.9.0
    - org.apache.log4j-2.17.0. #10


## [0.14.0] - 2021-12-14
### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - cdc-io-0.13.0
    - cdc-kernel-0.14.0
    - org.apache.log4j-2.16.0. #10


## [0.13.1] - 2021-10-03
### Changed
- Updated dependencies.

### Fixed
- Fixed warnings.


### Fixed
- invalid index and check in `KeyedTableDiff.getKey()`. #9


## [0.13.0] - 2021-10-01
### Added
- A new `NO_CELL_STYLES` feature was added to `WorkbookWriterFeatures`.  
  With POI, setting a style twice seems to be an issue.
  This new features may be used to let the user set styles. #7
- A new `SHOW_CHANGE_DETAILS` option was added to `KeyedSheetDiff`.  
  It is now possible to show both values on CHANGED cells.  #8

## Removed
- `DiffKind` has been removed and replaced by `CellDiffKind` and `RowDiffKind`. #7

### Fixed
- Colors were not working correctly in some cases with `KeyedSheetDiff`. #7 
- Computation of differences has been fixed. #7


## [0.12.3] - 2021-10-20
### Fixed
- Deployment issue with version 0.12.2.
  Replication to Maven Central took almost 20h.


## [0.12.2] - 2021-10-19
### Changed
- Renamed package `cdc.office.ss.tools` to `cdc.office.tools`. #5

### Fixed
- Infinite recursion in CsvParser.parse(). #6


## [0.12.1] - 2021-10-03
### Fixed
- Fixed regression in handling of colors in `KeyedTableDiffExporter`. #4


## [0.12.0] - 2021-10-02
### Added
- Created `KeyedTableDiffExporter`. cdc-java/cdc-applic#84
- Added `Row.LEXICOGRAPHIC_COMPARATOR`.
- Added `SheetParserFactory.Feature.EVALUATE_FORMULA`to force fresh evaluation
  of formula.  
  This is disabled by default. It can consume more CPU and memory.
  At the moment, it is only used in StandardPOI.  
  It should used with care.
  When not used, formatting of result may be incorrect. #3

### Changed
- `Rows.toExtract` handles negative numbers.

### Fixed
- When parsing sheets, returns value instead of formula when reading XLS
  or using STANDARD_POI. #3


## [0.11.0] - 2021-07-23
### Added
- Created `TablesHandler`.

### Changed
- Updated dependencies.
- Changed processBegin to processBeginTable and processEnd to processEndTable in TableHandler.
- Replaced String parameter with Charset in several places.
- Added systemId parameter and used TablesHandler in SheetParser.

### Fixed
- Now, when cell value is empty, returns def, in Row.getValue. #2


## [0.10.1] - 2021-06-05
### Fixed
- Interrupt office loading when asked by application. #1
- Do not force line wrap in ExcelWorkbookWriter.


## [0.10.0] - 2021-05-03
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#41
