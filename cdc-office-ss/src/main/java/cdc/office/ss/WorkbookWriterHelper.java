package cdc.office.ss;

import java.io.IOException;
import java.util.List;

import cdc.office.tables.Row;
import cdc.office.tables.TableSection;

/**
 * Helper class used to dump rows in several sheets if they don't hold in 1 sheet.
 * <p>
 * As many sheet as necessary are created, using a prefix, a number and a suffix.
 *
 * @param <W> The WorkbookWriter type.
 */
public class WorkbookWriterHelper<W extends WorkbookWriter<W>> {
    private final W writer;
    private int maxRowsPerSheet = -1;
    private String sheetPrefix;
    private String sheetSuffix;
    private WorkbookWriterConsumer<W> headerGenerator;
    private int sheetIndex;
    /** If true, indexing is enabled. */
    private final boolean indexing;

    /**
     * Creates a helper with or without indexing.
     *
     * @param writer The writer.
     * @param indexing The indexing.<br>
     *            <b>WARNING:</b> it should be disabled if one knows that result will hold on 1 sheet.
     */
    public WorkbookWriterHelper(W writer,
                                boolean indexing) {
        this.writer = writer;
        this.indexing = indexing;
        this.maxRowsPerSheet = writer.getKind().getMaxRows();
    }

    /**
     * Creates a Helper with indexing enabled.
     *
     * @param writer The writer.
     */
    public WorkbookWriterHelper(W writer) {
        this(writer, true);
    }

    /**
     * @return The underlying writer.
     */
    public W getWriter() {
        return writer;
    }

    /**
     * @return {@code true} if indexing is enabled: index in taken into account when creating sheets.
     */
    public boolean isIndexing() {
        return indexing;
    }

    /**
     * @return The current sheet prefix.
     */
    public String getSheetPrefix() {
        return sheetPrefix;
    }

    /**
     * @return The current sheet suffix.
     */
    public String getSheetSuffix() {
        return sheetSuffix;
    }

    /**
     * @return The current sheet index (for current prefix).
     */
    public int getSheetIndex() {
        return sheetIndex;
    }

    /**
     * @return The current effective maximum number of rows per sheet.
     */
    public int getMaxRowsPerSheet() {
        return maxRowsPerSheet;
    }

    public void beginSheets(String prefix,
                            String suffix,
                            WorkbookWriterConsumer<W> headerGenerator,
                            int maxRowsPerSheet) throws IOException {
        this.sheetPrefix = prefix == null ? "" : prefix;
        this.sheetSuffix = suffix == null ? "" : suffix;
        this.headerGenerator = headerGenerator;
        this.sheetIndex = 0;
        final int max = writer.getKind().getMaxRows();

        if (maxRowsPerSheet < 0) {
            this.maxRowsPerSheet = max;
        } else if (max < 0) {
            this.maxRowsPerSheet = maxRowsPerSheet;
        } else {
            this.maxRowsPerSheet = Math.min(maxRowsPerSheet, max);
        }
        beginNewSheet();
    }

    public void beginSheets(String prefix,
                            String suffix,
                            WorkbookWriterConsumer<W> headerGenerator) throws IOException {
        beginSheets(prefix,
                    suffix,
                    headerGenerator,
                    -1);
    }

    /**
     * Starts writing a set of sheets.
     * <p>
     * Sheets are created automatically when they are full.
     *
     * @param prefix The sheets prefix.
     * @param suffix The sheets suffix.
     * @param header The sheets header.
     * @param maxRowsPerSheet The requested maximum number of rows per sheet.
     * @throws IOException When an IO error occurs.
     */
    public void beginSheets(String prefix,
                            String suffix,
                            Row header,
                            int maxRowsPerSheet) throws IOException {
        beginSheets(prefix,
                    suffix,
                    w -> addRow(TableSection.HEADER, header),
                    maxRowsPerSheet);
    }

    /**
     * Starts writing a set of sheets.
     * <p>
     * Sheets are created automatically when they are full.
     *
     * @param prefix The sheets prefix.
     * @param suffix The sheets suffix.
     * @param header The sheets header.
     * @throws IOException When an IO error occurs.
     */
    public void beginSheets(String prefix,
                            String suffix,
                            Row header) throws IOException {
        beginSheets(prefix,
                    suffix,
                    header,
                    -1);
    }

    private void beginNewSheet() throws IOException {
        this.sheetIndex++;
        if (indexing || sheetIndex > 1) {
            writer.beginSheet(sheetPrefix + sheetIndex + sheetSuffix);
        } else {
            writer.beginSheet(sheetPrefix + sheetSuffix);
        }

        this.headerGenerator.accept(writer);
    }

    private void beginNewSheetIfFull() throws IOException {
        if (maxRowsPerSheet > 0 && writer.getNumberOfRowsInSheet() == maxRowsPerSheet) {
            beginNewSheet();
        }
    }

    private void addRow(TableSection section,
                        Row row) throws IOException {
        beginNewSheetIfFull();
        writer.addRow(section, row);
    }

    public void addRow(Row row) throws IOException {
        addRow(TableSection.DATA, row);
    }

    public void addRows(Row... rows) throws IOException {
        for (final Row row : rows) {
            addRow(row);
        }
    }

    public void addRows(List<Row> rows) throws IOException {
        for (final Row row : rows) {
            addRow(row);
        }
    }

    public void beginRow(TableSection section) throws IOException {
        beginNewSheetIfFull();
        writer.beginRow(section);
    }

    public void beginRow() throws IOException {
        beginRow(TableSection.DATA);
    }

    public void addEmptyCell() throws IOException {
        this.writer.addEmptyCell();
    }

    public void addEmptyCells(int count) throws IOException {
        this.writer.addEmptyCells(count);
    }

    public void addCell(Object object) throws IOException {
        this.writer.addCell(object);
    }

    public void addCells(Object... objects) throws IOException {
        this.writer.addCells(objects);
    }
}