package cdc.office.ss;

import java.io.IOException;

public interface WorkbookWriterConsumer<W extends WorkbookWriter<W>> {
    public void accept(W writer) throws IOException;
}