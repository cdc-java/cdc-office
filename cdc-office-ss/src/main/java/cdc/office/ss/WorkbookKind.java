package cdc.office.ss;

import java.io.File;
import java.net.URL;

import cdc.util.files.Files;

/**
 * Enumeration of supported workbook (spreadsheet) kinds.
 *
 * @author Damien Carbonne
 *
 */
public enum WorkbookKind {
    /** Csv file. */
    CSV("csv"),
    /** ODF Spreadsheet. */
    ODS("ods"),
    /** MS Excel. */
    XLS("xls"),
    /** MS Excel. */
    XLSM("xlsm"),
    /** MS Excel. */
    XLSX("xlsx");

    private final String extension;

    private WorkbookKind(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public static WorkbookKind from(File file) {
        final String ext = Files.getExtension(file).toLowerCase();
        for (final WorkbookKind kind : values()) {
            if (kind.extension.equals(ext)) {
                return kind;
            }
        }
        return null;
    }

    public static WorkbookKind from(URL url) {
        return from(new File(url.getFile()));
    }

    /**
     * @return The maximum number of rows supported by this kind of spreadsheet.
     */
    public int getMaxRows() {
        return switch (this) {
        case CSV -> -1;
        case XLS -> 65_536;
        case ODS, XLSM, XLSX -> 1_048_576;
        };
    }

    /**
     * @return The maximum number of columns supported by this kind of spreadsheet.
     */
    public int getMaxColumns() {
        return switch (this) {
        case CSV -> -1;
        case ODS -> 1024;
        case XLS -> 256;
        case XLSM, XLSX -> 16_384;
        };
    }

    /**
     * @return The maximum number of characters in a cell supported by this kind of spreadsheet.
     */
    public int getMaxCellSize() {
        return switch (this) {
        case CSV -> -1;
        case ODS, XLS, XLSM, XLSX -> 32_767;
        };
    }

    /**
     * @return The maximum number of characters in sheet name supported by this kind of spreadsheet.
     */
    public int getMaxSheetName() {
        return switch (this) {
        case CSV, ODS -> -1;
        case XLS, XLSM, XLSX -> 31;
        };
    }

    /**
     * @return The maximum number of characters for content validation message supported by this kind of spreadsheet.
     */
    public int getMaxContentValidationMessageSize() {
        return switch (this) {
        case CSV -> -1;
        case ODS, XLS, XLSM, XLSX -> 250;
        };
    }

    /**
     * @return The maximum number of characters (including separators) that can be used in a validation explicit list.
     */
    public int getMaxValidationExplicitListLength() {
        return switch (this) {
        case CSV -> 0;
        case ODS -> -1;
        case XLS, XLSM, XLSX -> 255;
        };
    }
}