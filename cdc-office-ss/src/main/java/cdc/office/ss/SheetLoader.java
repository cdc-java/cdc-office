package cdc.office.ss;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import cdc.office.tables.MemoryTableHandler;
import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.TablesHandler;
import cdc.office.tables.TablesMerger;
import cdc.util.function.Evaluation;

/**
 * Class used to load an Office sheet as a table.
 *
 * @author Damien Carbonne
 *
 */
public class SheetLoader {
    private final SheetParserFactory factory = new SheetParserFactory();

    public SheetLoader() {
        super();
    }

    public SheetParserFactory getFactory() {
        return factory;
    }

    /**
     * Loads a sheet from a file as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     * @param file The file to load.
     * @param password The password protecting {@code file}.
     * @param sheetName The sheet name. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(File file,
                          String password,
                          String sheetName) throws IOException {
        final SheetParser parser = factory.create(file);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(file, password, sheetName, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from a file as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     * @param file The file to load.
     * @param password The password protecting {@code file}.
     * @param sheetIndex The 0-based index of the sheet. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(File file,
                          String password,
                          int sheetIndex) throws IOException {
        final SheetParser parser = factory.create(file);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(file, password, sheetIndex, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads several sheets from a file as a list of rows.
     * <p>
     * Header of first sheet is kept. Headers of other sheets are removed.
     *
     * @param file The file to load.
     * @param password The password protecting {@code file}.
     * @param sheetNamePredicate The predicate of sheets to load and merge.
     * @return The sheets as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(File file,
                          String password,
                          Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(file);

        final MemoryTableHandler handler = new MemoryTableHandler();
        final TablesMerger merger = new TablesMerger(sheetNamePredicate, handler);
        try {
            parser.parse(file, password, 1, merger);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from an InputStream as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     *
     * @param in The InputStream to load.
     * @param systemId The system id.
     * @param kind The sheet kind.
     * @param password The password protecting {@code file}.
     * @param sheetName The sheet name. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(InputStream in,
                          String systemId,
                          WorkbookKind kind,
                          String password,
                          String sheetName) throws IOException {
        final SheetParser parser = factory.create(kind);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(in, systemId, kind, password, sheetName, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from an InputStream as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     *
     * @param in The InputStream to load.
     * @param systemId The system id.
     * @param kind The sheet kind.
     * @param password The password protecting {@code file}.
     * @param sheetIndex The 0-based index of the sheet. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(InputStream in,
                          String systemId,
                          WorkbookKind kind,
                          String password,
                          int sheetIndex) throws IOException {
        final SheetParser parser = factory.create(kind);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(in, systemId, kind, password, sheetIndex, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from an InputStream as a list of rows.
     * <p>
     * Header of first sheet is kept. Headers of other sheets are removed.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     * @param in The InputStream to load.
     * @param systemId The system id.
     * @param kind The sheet kind.
     * @param password The password protecting {@code file}.
     * @param sheetNamePredicate The predicate of sheets to load and merge.
     * @return The sheets as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(InputStream in,
                          String systemId,
                          WorkbookKind kind,
                          String password,
                          Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(kind);
        final MemoryTableHandler handler = new MemoryTableHandler();
        final TablesMerger merger = new TablesMerger(sheetNamePredicate, handler);
        try {
            parser.parse(in, systemId, kind, password, 1, merger);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from a file as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     * @param url The URL to load.
     * @param password The password protecting {@code file}.
     * @param sheetName The sheet name. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(URL url,
                          String password,
                          String sheetName) throws IOException {
        final SheetParser parser = factory.create(url);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(url, password, sheetName, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads a sheet from a file as a list of rows.
     * <p>
     * All kinds defined in {@link WorkbookKind} are supported.
     * <p>
     * If necessary, set SheetFactory parameters.
     *
     * @param url The URL to load.
     * @param password The password protecting {@code file}.
     * @param sheetIndex The 0-based index of the sheet. Used if file is a multi-sheet file.
     * @return The sheet as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(URL url,
                          String password,
                          int sheetIndex) throws IOException {
        final SheetParser parser = factory.create(url);
        final MemoryTableHandler handler = new MemoryTableHandler();
        try {
            parser.parse(url, password, sheetIndex, 0, handler);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Loads several sheets from a file as a list of rows.
     * <p>
     * Header of first sheet is kept. Headers of other sheets are removed.
     *
     * @param url The URL to load.
     * @param password The password protecting {@code file}.
     * @param sheetNamePredicate The predicate of sheets to load and merge.
     * @return The sheets as a list of rows.
     * @throws IOException When a IO error occurs.
     */
    public List<Row> load(URL url,
                          String password,
                          Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(url);

        final MemoryTableHandler handler = new MemoryTableHandler();
        final TablesMerger merger = new TablesMerger(sheetNamePredicate, handler);
        try {
            parser.parse(url, password, 1, merger);
            return handler.getRows();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    public List<String> getMatchingSheetNames(File file,
                                              String password,
                                              Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(file);

        final TablesMatcher matcher = new TablesMatcher(sheetNamePredicate);
        try {
            parser.parse(file, password, 1, matcher);
            return matcher.getSheetNames();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    public List<String> getMatchingSheetNames(InputStream in,
                                              String systemId,
                                              WorkbookKind kind,
                                              String password,
                                              Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(kind);

        final TablesMatcher matcher = new TablesMatcher(sheetNamePredicate);
        try {
            parser.parse(in, systemId, kind, password, 1, matcher);
            return matcher.getSheetNames();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    public List<String> getMatchingSheetNames(URL url,
                                              String password,
                                              Predicate<String> sheetNamePredicate) throws IOException {
        final SheetParser parser = factory.create(url);

        final TablesMatcher matcher = new TablesMatcher(sheetNamePredicate);
        try {
            parser.parse(url, password, 1, matcher);
            return matcher.getSheetNames();
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Implementation of {@link TablesHandler} used to collect tables whose name matches a predicate.
     */
    private static class TablesMatcher implements TablesHandler {
        private final Predicate<String> sheetNamePredicate;
        private final List<String> sheetNames = new ArrayList<>();

        public TablesMatcher(Predicate<String> sheetNamePredicate) {
            this.sheetNamePredicate = sheetNamePredicate;
        }

        /**
         * @return The collected sheet names.
         */
        public List<String> getSheetNames() {
            return sheetNames;
        }

        @Override
        public void processBeginTables(String systemId) {
            sheetNames.clear();
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            if (sheetNamePredicate.test(name)) {
                sheetNames.add(name);
            }
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            return Evaluation.PRUNE;
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            return Evaluation.PRUNE;
        }
    }
}