package cdc.office.ss.odf;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableCell;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.odftoolkit.odfdom.dom.attribute.office.OfficeValueTypeAttribute;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationElement;
import org.odftoolkit.odfdom.dom.element.table.TableTableCellElementBase;
import org.odftoolkit.odfdom.dom.element.text.TextAElement;
import org.odftoolkit.odfdom.dom.element.text.TextPElement;

import cdc.office.ss.ContentValidation;
import cdc.office.ss.Section;
import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.lang.Checks;
import cdc.util.lang.DateUtils;
import cdc.util.strings.StringUtils;

/**
 * Open Office implementation of WorkbookWriter using ODF Toolkit.
 *
 * @author Damien Carbonne
 */
public class OdsWorkbookWriter implements WorkbookWriter<OdsWorkbookWriter> {
    private static final Logger LOGGER = LogManager.getLogger(OdsWorkbookWriter.class);
    private static final Pattern NL_PATTERN = Pattern.compile("\\R");

    private final File file;
    private final OutputStream out;
    private final WorkbookWriterFeatures features;
    private Section section = Section.WORKBOOK;
    private final OdfSpreadsheetDocument doc;
    /** Current table (sheet). */
    private OdfTable table;
    /** Current row. */
    private OdfTableRow row;
    /** Current row index (0-based). */
    private int rowIndex = -1;
    /** Current cell. */
    private OdfTableCell cell;
    /** Current column index (0-based). */
    private int columnIndex = -1;

    private int numberOfSheets = 0;

    private final String formatInt;
    private static final String FORMAT_DATE_TIME = "yyyy/MM/dd HH:mm:ss";
    private static final String FORMAT_DATE = "yyyy/MM/dd";
    private static final String FORMAT_TIME = "HH:mm:ss";

    private OdsWorkbookWriter(File file,
                              OutputStream out,
                              WorkbookKind kind,
                              WorkbookWriterFeatures features)
            throws IOException {
        this.file = file;
        this.out = out;
        this.features = features;
        if (kind != WorkbookKind.ODS) {
            throw new IllegalArgumentException();
        }
        try {
            this.doc = OdfSpreadsheetDocument.newSpreadsheetDocument();
            // We don't want any sheet to be created automatically
            for (final OdfTable t : doc.getTableList(false)) {
                t.remove();
            }
        } catch (final Exception e) {
            throw new IOException(e);
        }
        this.table = null;
        this.row = null;
        this.cell = null;

        if (this.features.isEnabled(WorkbookWriterFeatures.Feature.USE_THOUSANDS_SEPARATOR)) {
            this.formatInt = "#,##0";
        } else {
            this.formatInt = "#";
        }
    }

    public OdsWorkbookWriter(File file,
                             WorkbookWriterFeatures features)
            throws IOException {
        this(file, null, WorkbookKind.from(file), features);
    }

    public OdsWorkbookWriter(OutputStream out,
                             WorkbookWriterFeatures features)
            throws IOException {
        this(null, out, WorkbookKind.ODS, features);
    }

    public OdsWorkbookWriter(File file,
                             WorkbookWriterFeatures features,
                             WorkbookWriterFactory factory)
            throws IOException {
        this(file,
             features);
    }

    public OdsWorkbookWriter(OutputStream out,
                             WorkbookKind kind,
                             WorkbookWriterFeatures features,
                             WorkbookWriterFactory factory)
            throws IOException {
        this(null, out, kind, features);
    }

    @Override
    public OdsWorkbookWriter self() {
        return this;
    }

    @Override
    public WorkbookKind getKind() {
        return WorkbookKind.ODS;
    }

    @Override
    public WorkbookWriterFeatures getFeatures() {
        return features;
    }

    @Override
    public boolean isSupported(WorkbookWriterFeatures.Feature feature) {
        return feature == WorkbookWriterFeatures.Feature.USE_THOUSANDS_SEPARATOR
                || feature == WorkbookWriterFeatures.Feature.TRUNCATE_CELLS
                || feature == WorkbookWriterFeatures.Feature.TRUNCATE_CELLS_LINES;
    }

    @Override
    public int getNumberOfSheets() {
        return numberOfSheets;
    }

    @Override
    public int getNumberOfRowsInSheet() {
        return numberOfSheets == 0
                ? -1
                : rowIndex + 1;
    }

    @Override
    public int getNumberOfCellsInRow() {
        return numberOfSheets == 0 || rowIndex < 0
                ? -1
                : columnIndex + 1;
    }

    private void unexpectedState(String context) throws IOException {
        throw new IOException("Unexpected state " + section + " in " + context);
    }

    public OdfSpreadsheetDocument getDocument() {
        return doc;
    }

    public OdfTable getTable() {
        return table;
    }

    public OdfTableRow getRow() {
        return row;
    }

    public OdfTableCell getCell() {
        return cell;
    }

    @Override
    public OdsWorkbookWriter beginSheet(String name) {
        this.table = OdfTable.newTable(doc);
        this.table.setTableName(name);
        Checks.assertFalse(table == null, "Null table");
        numberOfSheets++;
        this.rowIndex = -1;
        this.columnIndex = -1;
        this.row = null;
        this.cell = null;
        this.section = Section.SHEET;
        return this;
    }

    @Override
    public OdsWorkbookWriter addContentValidation(ContentValidation cv) throws IOException {
        LOGGER.warn("addContentValidation(...) NYI");
        // TODO
        return this;
    }

    @Override
    public OdsWorkbookWriter beginRow(TableSection section) throws IOException {
        if (this.section == Section.WORKBOOK) {
            unexpectedState("beginRow");
        }
        if (section == TableSection.DATA) {
            if (this.section == Section.HEADER_CELL && features.isEnabled(WorkbookWriterFeatures.Feature.AUTO_FILTER_COLUMNS)) {
                LOGGER.warn("AUTO_FILTER_COLUMNS NYI");
                // TODO
                // this.doc.addAutoFilter(AutoFilter.builder("Range" + tableCount, this.table, rowIndex, 0, rowIndex, columnIndex)
                // .build());
            }
            this.section = Section.DATA_ROW;
        } else {
            this.section = Section.HEADER_ROW;
        }

        this.rowIndex++;
        this.row = null;
        this.cell = null;
        this.columnIndex = -1;
        return this;
    }

    public void addCell() throws IOException {
        if (section == Section.WORKBOOK || section == Section.SHEET) {
            unexpectedState("addCell");
        }
        if (this.row == null) {
            this.row = table.getRowByIndex(this.rowIndex);
        }
        this.columnIndex++;
        this.cell = row.getCellByIndex(this.columnIndex);
        if (features.isEnabled(WorkbookWriterFeatures.Feature.WRAP_TEXT)) {
            this.cell.setTextWrapped(true);
        }

        if (section == Section.DATA_ROW) {
            this.section = Section.DATA_CELL;
        } else if (section == Section.HEADER_ROW) {
            this.section = Section.HEADER_CELL;
        }
    }

    @Override
    public OdsWorkbookWriter addCellComment(String comment) {
        // The length of the longest line
        final int maxLineLength = StringUtils.maxLineLength(comment);
        // The number of lines
        final int numberOfLines = StringUtils.numberOfLines(comment);
        // No more than 200 characters
        final int maxLength = 200;
        // No more than 50 lines
        final int maxLines = 50;
        // Should be computed
        final double widthDivisior = 4.0;
        // Should be computed
        final double heightDivisior = 3.0;

        final float width = (float) (Math.min(maxLineLength, maxLength) / widthDivisior);
        final float height = (float) (Math.min(numberOfLines, maxLines) / heightDivisior);

        final TableTableCellElementBase base = cell.getOdfElement();
        final OfficeAnnotationElement annotation = base.newOfficeAnnotationElement();
        annotation.setSvgWidthAttribute(width + "cm");
        annotation.setSvgHeightAttribute(height + "cm");
        annotation.setSvgXAttribute("3cm");
        annotation.setSvgYAttribute("0cm");
        annotation.setDrawCaptionPointXAttribute("-0.5cm");
        annotation.setDrawCaptionPointYAttribute("0cm");

        for (final String s : NL_PATTERN.split(comment)) {
            final TextPElement p = annotation.newTextPElement();
            p.setTextContent(s);
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addEmptyCell() throws IOException {
        this.columnIndex++;
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(boolean value) throws IOException {
        addCell();
        cell.setBooleanValue(value);
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(String value) throws IOException {
        if (value == null) {
            addEmptyCell();
        } else {
            addCell();
            if (features.isEnabled(WorkbookWriterFeatures.Feature.TRUNCATE_CELLS)) {
                cell.setStringValue(StringUtils.extract(value, getKind().getMaxCellSize()));
            } else if (features.isEnabled(WorkbookWriterFeatures.Feature.TRUNCATE_CELLS_LINES)) {
                cell.setStringValue(StringUtils.extractAverage(value, features.getMaxLineLength(), getKind().getMaxCellSize()));
            } else {
                cell.setStringValue(value);
            }
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(double value) throws IOException {
        addCell();
        cell.setDoubleValue(value);
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(long value) throws IOException {
        addCell();
        cell.setDoubleValue((double) value);
        cell.setFormatString(formatInt);
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(Date value) throws IOException {
        if (value == null) {
            addEmptyCell();
        } else {
            addCell();
            cell.setDateValue(DateUtils.asCalendar(value));
            cell.setFormatString(FORMAT_DATE_TIME);
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(LocalDateTime value) throws IOException {
        if (value == null) {
            addEmptyCell();
        } else {
            addCell();
            cell.setDateValue(DateUtils.asCalendar(value));
            cell.setFormatString(FORMAT_DATE_TIME);
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(LocalDate value) throws IOException {
        if (value == null) {
            addEmptyCell();
        } else {
            addCell();
            cell.setDateValue(DateUtils.asCalendar(value));
            cell.setFormatString(FORMAT_DATE);
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(LocalTime value) throws IOException {
        if (value == null) {
            addEmptyCell();
        } else {
            addCell();
            cell.setTimeValue(DateUtils.asCalendar(value));
            cell.setFormatString(FORMAT_TIME);
        }
        return this;
    }

    @Override
    public OdsWorkbookWriter addCell(URI uri,
                                     String label) throws IOException {
        if (uri == null) {
            addEmptyCell();
        } else {
            addCell();
            final TableTableCellElementBase base = cell.getOdfElement();
            base.setOfficeValueTypeAttribute(OfficeValueTypeAttribute.Value.STRING.toString());
            final TextPElement p;
            if (base.getFirstChildElement() != null) {
                p = (TextPElement) base.getFirstChildElement();
            } else {
                p = base.newTextPElement();
            }
            final TextAElement a = p.newTextAElement(uri.toURL().toString(), "simple");
            a.setTextContent(label);
        }
        return this;
    }

    @Override
    public void flush() throws IOException {
        // Ignore (not supported)
    }

    @Override
    public void close() throws IOException {
        try {
            if (file == null) {
                this.doc.save(out);
            } else {
                this.doc.save(file);
            }
            this.doc.close();
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }
}