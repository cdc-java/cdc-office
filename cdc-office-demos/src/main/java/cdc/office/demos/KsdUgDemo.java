package cdc.office.demos;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.office.csv.CsvWriter;
import cdc.office.tables.Row;
import cdc.office.tools.KeyedSheetDiff;

public final class KsdUgDemo {
    private static final Logger LOGGER = LogManager.getLogger(KsdUgDemo.class);

    private static class CsvBuilder {
        private final File file;
        private final List<Row> rows = new ArrayList<>();

        public CsvBuilder(File file) {
            this.file = file;
        }

        public CsvBuilder row(String... values) {
            rows.add(Row.builder().addValues(values).build());
            return this;
        }

        public File build() throws IOException {
            try (final CsvWriter writer = new CsvWriter(file, StandardCharsets.UTF_8)) {
                writer.setSeparator(';');
                boolean first = true;
                for (final Row row : rows) {
                    if (first) {
                        first = false;
                    } else {
                        writer.writeln();
                    }
                    writer.write(row.getValues());
                }
                writer.flush();
                return file;
            }
        }
    }

    private static final File FILE1 = new File("target/ksd-ug-file1.csv");
    private static final File FILE2 = new File("target/ksd-ug-file2.csv");

    private static void buildFile1() throws IOException {
        LOGGER.info("Build {}", FILE1);
        final CsvBuilder builder = new CsvBuilder(FILE1);
        builder.row("ID", "A", "B", "C", "D")
               .row("0", "Hello", "1", "10.0", "World")
               .row("1", "Hello", "1", "10.0", "World")
               .row("2", "Hello", "1", "11.0", "World")
               .build();
    }

    private static void buildFile2() throws IOException {
        LOGGER.info("Build {}", FILE2);
        final CsvBuilder builder = new CsvBuilder(FILE2);
        builder.row("ID", "A", "B", "C", "D")
               .row("1", "Hello", "1", "10.0", "World")
               .row("2", "Hello", "1", "10.0", "")
               .row("3", "Hello", "1", "10.0", "World")
               .build();
    }

    private static void generate(String suffix,
                                 Consumer<KeyedSheetDiff.MainArgs> margsConsumer) throws IOException {
        final KeyedSheetDiff.MainArgs margs = new KeyedSheetDiff.MainArgs();
        margs.features.add(KeyedSheetDiff.MainArgs.Feature.VERBOSE);
        margs.keys.add("ID");
        margs.file1 = FILE1;
        margs.file2 = FILE2;
        margsConsumer.accept(margs);

        margs.output = new File("target", "ksd-ug-" + suffix + ".csv");
        LOGGER.info("Generate {}", margs.output);
        KeyedSheetDiff.execute(margs);

        margs.output = new File("target", "ksd-ug-" + suffix + ".xlsx");
        LOGGER.info("Generate {}", margs.output);
        KeyedSheetDiff.execute(margs);
    }

    public static void main(String[] args) throws IOException {
        buildFile1();
        buildFile2();
        generate("basic",
                 margs -> {
                     // Ignore
                 });
        generate("split",
                 margs -> margs.features.add(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS));
        generate("cell-diffs",
                 margs -> margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS));
        generate("cell-diffs-original-names",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_ORIGINAL_NAMES);
                 });
        generate("split-cell-diffs-original-names",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_ORIGINAL_NAMES);
                 });
        generate("split-cell-diffs-original-names-file-marks",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_ORIGINAL_NAMES);
                     margs.file1Mark = "(File 1)";
                     margs.file2Mark = "(File 2)";
                 });
        generate("line-diff",
                 margs -> margs.features.add(KeyedSheetDiff.MainArgs.Feature.LINE_DIFF_COLUMN));
        generate("no-line-diff",
                 margs -> margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_LINE_DIFF_COLUMN));
        generate("attribute",
                 margs -> {
                     margs.attributes.add("A");
                     margs.attributes.add("B");
                 });
        generate("map",
                 margs -> {
                     margs.keys.clear();
                     margs.keys.add("id");
                     margs.map1.put("ID", "id");
                     margs.map1.put("A", "a");
                     margs.map1.put("B", "B");
                     margs.map2.put("ID", "id");
                     margs.map2.put("A", "a");
                     margs.map2.put("B", "B");
                 });
        generate("no-added-or-removed-marks",
                 margs -> margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_ADDED_OR_REMOVED_MARKS));
        generate("diff-mark",
                 margs -> margs.diffMark = "DIFF");
        generate("diff-mark-show-change-details",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                 });
        generate("diff-mark-show-change-details-sort-no-unchanged",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_UNCHANGED_LINES);
                 });
        generate("diff-mark-show-change-details-sort-no-unchanged-no-added-or-removed-marks",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SORT_LINES);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_UNCHANGED_LINES);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.NO_ADDED_OR_REMOVED_MARKS);
                 });
        generate("cell-diff-columns-show-change-details",
                 margs -> {
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS);
                     margs.features.add(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS);
                 });
    }
}