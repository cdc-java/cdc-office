package cdc.office.tables;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Rows utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class Rows {
    private Rows() {
    }

    /**
     * Returns a string containing at most max columns of a row.
     *
     * @param row The row.
     * @param maxColumns The max number of columns to display.<br>
     *            A negative number means there is no filtering.
     * @return A string representation of {@code row} containing at most {@code maxColumns} columns.
     */
    public static String toExtract(Row row,
                                   int maxColumns) {
        final StringBuilder builder = new StringBuilder();
        for (int column = 0; (maxColumns < 0 || column < maxColumns) && column < row.size(); column++) {
            builder.append(" '");
            builder.append(row.getValue(column));
            builder.append('\'');
        }
        if (row.size() > maxColumns) {
            builder.append(" ...");
        }
        return builder.toString();
    }

    /**
     * Returns a {@link Row} that only contains data corresponding to names present in a set.
     * <p>
     * For example:
     * <ul>
     * <li>{@code header = ["A", "B", "C"]}
     * <li>{@code keep = ["A", "C"]}
     * <li>{@code filter(["x", "y", "z"], header, keep) = ["x", "z"]}
     * </ul>
     *
     * @param row The row to filter.
     * @param header The associated header.
     * @param keep The set of names to keep.
     * @return A new row containing at most kept cells.
     */
    public static Row filter(Row row,
                             Header header,
                             Set<String> keep) {
        final Row.Builder buffer = Row.builder();
        final int max = Math.min(row.size(), header.size());
        for (int index = 0; index < max; index++) {
            final String name = header.getNameAt(index);
            if (keep.contains(name)) {
                buffer.addValue(row.getValue(index));
            }
        }
        return buffer.build();
    }

    /**
     * Returns a modifiable list of filtered rows applying {@link #filter(Row, Header, Set)}.
     *
     * @param rows The rows to filter.
     * @param header The associated header.
     * @param keep The set of names to keep.
     * @return A new list or filtered rows.
     */
    public static List<Row> filter(List<Row> rows,
                                   Header header,
                                   Set<String> keep) {
        return rows.stream()
                   .map(r -> filter(r, header, keep))
                   .collect(Collectors.toList());
    }

    public static void fixHeader(List<Row> rows,
                                 String prefix) {
        if (!rows.isEmpty()) {
            final Row row = rows.get(0).fixNullAndDuplicates(prefix);
            rows.set(0, row);
        }
    }

    /**
     * Renames the first row.
     *
     * @param rows The list of rows.
     * @param map The map to apply.
     */
    public static void renameHeader(List<Row> rows,
                                    Map<String, String> map) {
        if (!map.isEmpty() && !rows.isEmpty()) {
            final Row row = rename(rows.get(0), map);
            rows.set(0, row);
        }
    }

    /**
     * Renames a row using a map.
     * <p>
     * If a name is mapped, then it is converted.
     * Otherwise, it is left unchanged.
     *
     * @param row The row.
     * @param map The map to apply.
     * @return The renaming of {@code row}.
     */
    public static Row rename(Row row,
                             Map<String, String> map) {
        final Row.Builder builder = Row.builder();
        for (final String name : row.getValues()) {
            builder.addValue(map.getOrDefault(name, name));
        }
        return builder.build();
    }
}