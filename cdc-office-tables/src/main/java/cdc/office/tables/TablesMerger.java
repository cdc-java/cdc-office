package cdc.office.tables;

import java.util.function.Predicate;

import cdc.util.function.Evaluation;

/**
 * Implementation of {@link TablesHandler} that can merge several tables into one.
 * <p>
 * Header of first table is kept, if it exists.
 */
public class TablesMerger implements TablesHandler {
    private final Predicate<String> sheetNamePredicate;
    private final MemoryTableHandler buffer = new MemoryTableHandler(true);
    private final TableHandler delegate;
    private Evaluation evaluation = Evaluation.PRUNE;
    private int count = 0;
    private final RowLocation.Builder locationBuilder = RowLocation.builder();

    public TablesMerger(Predicate<String> sheetNamePredicate,
                        TableHandler delegate) {
        this.sheetNamePredicate = sheetNamePredicate;
        this.delegate = delegate;
    }

    @Override
    public void processBeginTables(String systemId) {
        this.evaluation = Evaluation.PRUNE;
        this.count = 0;
    }

    @Override
    public void processEndTables(String systemId) {
        if (count >= 1) {
            this.delegate.processEndTable(null);
        }
    }

    @Override
    public void processBeginTable(String name,
                                  int numberOfRows) {
        if (sheetNamePredicate.test(name)) {
            count++;
            buffer.processBeginTable(null, -1);

            this.evaluation = Evaluation.CONTINUE;
            if (count == 1) {
                this.delegate.processBeginTable(null, -1);
            }
        } else {
            this.evaluation = Evaluation.PRUNE;
        }
    }

    @Override
    public void processEndTable(String name) {
        if (evaluation == Evaluation.CONTINUE) {
            // This will removed empty trailing rows
            buffer.processEndTable(null);

            // Transfer buffer into delegate
            for (int i = 0; i < buffer.getRowsCount(); i++) {
                locationBuilder.incrementNumbers(TableSection.DATA);
                delegate.processData(buffer.getRow(i), locationBuilder.build());
            }

            // Clear buffer
            buffer.clear();
        }
    }

    @Override
    public Evaluation processHeader(Row header,
                                    RowLocation location) {
        if (evaluation.isContinue() && count == 1) {
            // We keep header of first table
            // It is not saved into buffer
            locationBuilder.incrementNumbers(TableSection.HEADER);
            delegate.processHeader(header, locationBuilder.build());
        }
        return evaluation;
    }

    @Override
    public Evaluation processData(Row data,
                                  RowLocation location) {
        if (evaluation.isContinue()) {
            // Save data into buffer
            buffer.processData(data, location);
        }
        return evaluation;
    }
}