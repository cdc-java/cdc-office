package cdc.office.tables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cdc.office.tables.HeaderCell.NameCell;
import cdc.office.tables.HeaderCell.PatternCell;
import cdc.util.lang.Checks;

/**
 * Helper class used to map expected (valid mandatory and valid optional) header to an actual (possibly invalid) one.
 * <p>
 * This class checks that :
 * <ul>
 * <li>Some mandatory/optional cells are missing or not.
 * <li>Some actual names are additional or not.
 * </ul>
 * It does not check that order of actual header matches, in some way, an expected order.
 *
 * @author Damien Carbonne
 */
public final class HeaderMapper {
    /** The mandatory header. */
    private final Header mandatoryHeader;
    /** The optional header. */
    private final Header optionalHeader;
    /** The actual header. Shall not contain patterns. */
    private final Header actualHeader;
    /** The missing mandatory cells. */
    private final Set<HeaderCell> missingMandatoryCells;
    /** The missing optional cells. */
    private final Set<HeaderCell> missingOptionalCells;
    /** The actual mandatory names. */
    private final Set<String> actualMandatoryNames;
    /** The actual optional names. */
    private final Set<String> actualOptionalNames;
    /** The actual additional names. */
    private final Set<String> additionalNames;
    /** The actual names corresponding to each expected cell header. */
    private final Map<HeaderCell, List<String>> expectedToActuals = new HashMap<>();

    /**
     * Creates a header mapper from a mandatory, optional and actual headers.
     *
     * @param builder The Builder.
     * @throws IllegalArgumentException When {@code mandatory} is {@code null} or invalid,<br>
     *             or when {@code optional} is {@code null} or invalid,<br>
     *             or when {@code actual} is {@code null},<br>
     *             or when {@code mandatory} and {@code optional} intersect each other,<br>
     *             or when an {@code actual} cell matches several mandatory or optional cells.
     */
    private HeaderMapper(Builder builder) {
        this.mandatoryHeader = Checks.isNotNull(builder.mandatory, "mandatory");
        Checks.isTrue(builder.mandatory.isValid(), "Invalid mandatory header.");

        this.optionalHeader = Checks.isNotNull(builder.optional, "optional");
        Checks.isTrue(builder.optional.isValid(), "Invalid optional header.");

        Checks.isFalse(builder.mandatory.intersects(builder.optional), "Mandatory and optional headers can not intersect.");

        this.actualHeader = Checks.isNotNull(builder.actual, "actual");
        Checks.isTrue(builder.actual.hasOnlyNames(), "Actual header can not contain patterns.");

        for (final String name : actualHeader.getDeclaredNames()) {
            final int mandatoryCount = mandatoryHeader.getMatchingCount(name);
            final int optionalCount = optionalHeader.getMatchingCount(name);
            final int count = mandatoryCount + optionalCount;
            Checks.isFalse(count > 1, "Actual '{}' matches too many ({}) mandatory/optional cells.", name, count);
        }

        // Set of mandatory header cells that are missing
        // Add all mandatory cells, then remove those that are found
        final Set<HeaderCell> missingMandatorySet = new HashSet<>(builder.mandatory.getSortedCells());
        // Set of actual names that match mandatory header cell
        final Set<String> actualMandatorySet = new HashSet<>();

        for (final String name : actualHeader.getDeclaredNames()) {
            final Optional<HeaderCell> matching = mandatoryHeader.getMatchingCell(name);
            if (matching.isPresent()) {
                missingMandatorySet.remove(matching.get());
                actualMandatorySet.add(name);
                final List<String> list = expectedToActuals.computeIfAbsent(matching.get(), k -> new ArrayList<>());
                list.add(name);
            }
        }
        this.missingMandatoryCells = Collections.unmodifiableSet(missingMandatorySet);
        this.actualMandatoryNames = Collections.unmodifiableSet(actualMandatorySet);

        // Set of optional header cells that are missing
        // Add all optional cells, then remove those that are found
        final Set<HeaderCell> missingOptionalSet = new HashSet<>(builder.optional.getSortedCells());
        // Set of actual names that match optional header cell
        final Set<String> actualOptionalSet = new HashSet<>();

        for (final String name : actualHeader.getDeclaredNames()) {
            final Optional<HeaderCell> matching = optionalHeader.getMatchingCell(name);
            if (matching.isPresent()) {
                missingOptionalSet.remove(matching.get());
                actualOptionalSet.add(name);
                final List<String> list = expectedToActuals.computeIfAbsent(matching.get(), k -> new ArrayList<>());
                list.add(name);
            }
        }
        this.missingOptionalCells = Collections.unmodifiableSet(missingOptionalSet);
        this.actualOptionalNames = Collections.unmodifiableSet(actualOptionalSet);

        final Set<String> additionalSet = new HashSet<>();
        additionalSet.addAll(builder.actual.getDeclaredNames());
        additionalSet.removeAll(this.actualMandatoryNames);
        additionalSet.removeAll(this.actualOptionalNames);
        this.additionalNames = Collections.unmodifiableSet(additionalSet);
    }

    /**
     * @return The mandatory expected header.
     */
    public Header getMandatoryHeader() {
        return mandatoryHeader;
    }

    /**
     * @return The optional expected header.
     */
    public Header getOptionalHeader() {
        return optionalHeader;
    }

    /**
     * @return The actual header, possibly invalid.
     */
    public Header getActualHeader() {
        return actualHeader;
    }

    /**
     * @return A set of expected mandatory cells.
     */
    public Set<HeaderCell> getExpectedMandatoryCells() {
        return mandatoryHeader.getDeclaredCells();
    }

    /**
     * @return A set of missing mandatory cells.
     */
    public Set<HeaderCell> getMissingMandatoryCells() {
        return missingMandatoryCells;
    }

    /**
     * @return A set of actual mandatory names.
     */
    public Set<String> getActualMandatoryNames() {
        return actualMandatoryNames;
    }

    /**
     * @return {@code true} when there are missing mandatory cells.
     */
    public boolean hasMissingMandatoryCells() {
        return !missingMandatoryCells.isEmpty();
    }

    /**
     * @return {@code true} when all mandatory cells are there.<br>
     *         <b>Note</b> that mandatory header may however be invalid.
     */
    public boolean hasAllMandatoryCells() {
        return missingMandatoryCells.isEmpty();
    }

    /**
     * @return A set of expected optional cells.
     */
    public Set<HeaderCell> getExpectedOptionalCells() {
        return optionalHeader.getDeclaredCells();
    }

    /**
     * @return A set of missing optional cells.
     */
    public Set<HeaderCell> getMissingOptionalCells() {
        return missingOptionalCells;
    }

    /**
     * @return A set of actual optional names.
     */
    public Set<String> getActualOptionalNames() {
        return actualOptionalNames;
    }

    /**
     * @return {@code true} when there are missing optional cells.
     */
    public boolean hasMissingOptionalCells() {
        return !missingOptionalCells.isEmpty();
    }

    /**
     * @return A set of additional actual names.<br>
     *         They are neither optional nor mandatory.
     */
    public Set<String> getAdditionalNames() {
        return additionalNames;
    }

    /**
     * @return {@code true} when there are additional actual names.
     *         They are neither optional nor mandatory.
     */
    public boolean hasAdditionalNames() {
        return !getAdditionalNames().isEmpty();
    }

    /**
     * Returns the list of actual names that match an expected header cell.
     * <p>
     * For a {@link NameCell}, the list should be empty or contain 1 value.<br>
     * For a {@link PatternCell}, the list can have any size.
     *
     * @param expected The expected (mandatory of optional) header cell.
     * @return The list of actual names matching {@code expected}.
     * @throws IllegalArgumentException When {@code expected} is {@code null} or unknown.
     */
    public List<String> getActuals(HeaderCell expected) {
        Checks.isNotNull(expected, "expected");
        Checks.isTrue(mandatoryHeader.contains(expected) || optionalHeader.contains(actualHeader),
                      "Unkown header cell {}",
                      expected);

        return expectedToActuals.getOrDefault(expected, Collections.emptyList());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Header mandatory;
        private Header optional = Header.EMPTY;
        private Header actual;

        private Builder() {
        }

        public Builder mandatory(Header mandatory) {
            this.mandatory = mandatory;
            return this;
        }

        public Builder optional(Header optional) {
            this.optional = optional;
            return this;
        }

        public Builder actual(Header actual) {
            this.actual = actual;
            return this;
        }

        public HeaderMapper build() {
            return new HeaderMapper(this);
        }
    }
}