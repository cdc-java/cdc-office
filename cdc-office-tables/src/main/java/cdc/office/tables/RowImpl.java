package cdc.office.tables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of Row.
 *
 * @author Damien Carbonne
 *
 */
final class RowImpl implements Row {
    private final List<String> values;

    RowImpl(List<? extends Object> values) {
        final List<String> tmp = new ArrayList<>();
        for (final Object value : values) {
            tmp.add(value == null ? null : value.toString());
        }
        this.values = Collections.unmodifiableList(tmp);
    }

    @Override
    public List<String> getValues() {
        return values;
    }

    @Override
    public Row fixNullAndDuplicates(String prefix) {
        final List<String> list = new ArrayList<>();
        final Set<String> used = new HashSet<>(values);
        final Set<String> done = new HashSet<>();
        for (final String value : values) {
            String s = null;
            if (value == null || value.isEmpty() || done.contains(value)) {
                int index = 0;
                boolean found = false;
                do {
                    final String x = prefix + index;
                    if (!done.contains(x) && !used.contains(x)) {
                        found = true;
                        s = x;
                    }
                    index++;
                } while (!found);
            } else {
                s = value;
            }
            list.add(s);
            done.add(s);
        }
        return new RowImpl(list);
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof RowImpl)) {
            return false;
        }
        final RowImpl other = (RowImpl) object;
        return values.equals(other.values);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        builder.append('[');
        for (int column = 0; column < size(); column++) {
            if (column > 0) {
                builder.append(';');
            }
            final String value = getValue(column, null);
            if (value == null) {
                builder.append("null");
            } else {
                builder.append("'")
                       .append(value)
                       .append("'");

            }
        }
        builder.append(']');

        return builder.toString();
    }
}