package cdc.office.tables;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import cdc.office.tables.HeaderCell.NameCell;
import cdc.office.tables.HeaderCell.PatternCell;
import cdc.util.lang.Checks;

/**
 * Base interface of header cells. It can be:
 * <ul>
 * <li>a {@link NameCell simple name}, matching exactly one column.
 * <li>a {@link PatternCell pattern}, matching any number of columns.
 * </ul>
 *
 * @author Damien Carbonne
 */
public sealed interface HeaderCell permits NameCell, PatternCell {
    /**
     * @return The header label. It is its name or its pattern.
     */
    public String getLabel();

    /**
     * @return {@code true} if this header is a name.
     */
    public default boolean isName() {
        return this instanceof NameCell;
    }

    /**
     * @return {@code true} if this header is a pattern.
     */
    public default boolean isPattern() {
        return this instanceof PatternCell;
    }

    /**
     * @param text The text.
     * @return {@code true} if this cell matches {@code text}.
     */
    public boolean matches(String text);

    /**
     * @param name The name cell.
     * @return {@code true} if this cell matches {@code name}.
     */
    public default boolean matches(NameCell name) {
        return matches(name.getName());
    }

    /**
     * @param name The name.
     * @return A new {@link NameCell}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public static NameCell name(String name) {
        return new NameCell(name);
    }

    /**
     * @param regex The regular expression.
     * @return A new {@link PatternCell}.
     * @throws IllegalArgumentException When {@code regex} is {@code null}.
     * @throws PatternSyntaxException When {@code regex} can not be compiled.
     */
    public static PatternCell pattern(String regex) {
        return new PatternCell(regex);
    }

    /**
     * Implementation of {@link HeaderCell} used to represent exactly one column.
     *
     * @author Damien Carbonne
     * @param name The name.
     */
    public static record NameCell(String name) implements HeaderCell {
        public NameCell {
            Checks.isNotNull(name, "name");
        }

        @Override
        public String getLabel() {
            return "name:'" + name + "'";
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean matches(String text) {
            return name.equals(text);
        }

        @Override
        public String toString() {
            return name;
        }
    }

    /**
     * Implementation of {@link HeaderCell} used to represent several columns.
     *
     * @author Damien Carbonne
     */
    public static final class PatternCell implements HeaderCell {
        private final Pattern pattern;

        PatternCell(String regex) {
            this.pattern = Pattern.compile(Checks.isNotNull(regex, "regex"));
        }

        @Override
        public String getLabel() {
            return "pattern:'" + pattern.pattern() + "'";
        }

        public Pattern getPattern() {
            return pattern;
        }

        @Override
        public boolean matches(String text) {
            return pattern.matcher(text).matches();
        }

        @Override
        public int hashCode() {
            // WARNING: Pattern.hasCode does not work
            return pattern.pattern().hashCode();
        }

        @Override
        public boolean equals(Object object) {
            // WARNING: Pattern.equals does not work
            if (this == object) {
                return true;
            }
            if (!(object instanceof PatternCell)) {
                return false;
            }
            final PatternCell other = (PatternCell) object;
            return pattern.pattern().equals(other.pattern.pattern());
        }

        @Override
        public String toString() {
            return pattern.pattern();
        }
    }
}