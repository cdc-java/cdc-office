package cdc.office.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.office.tables.HeaderCell.NameCell;
import cdc.office.tables.HeaderCell.PatternCell;
import cdc.util.lang.Checks;

/**
 * Class used to represent a header: an ordered list of {@link HeaderCell cells}.
 * <p>
 * A cell can represent one ({@link NameCell}) or several ({@link PatternCell}) columns.<br>
 * A actual header shall only contain names, no patterns.
 * <p>
 * Some checks are done during construction, but one can not fully guarantee that a header is valid:
 * <ul>
 * <li>A header can not contain duplicate names.
 * <li>A pattern can not match a name
 * <li>However, one can not tell whether 2 patterns match same values or not.
 * </ul>
 * Examples:
 * <ul>
 * <li>[name("A"), name("A")] is detected as invalid: duplicate names.
 * <li>[pattern("."), pattern(".")]} is detected as invalid: duplicate patterns.
 * <li>[pattern("."), name("A")]} is detected as invalid: name matching pattern.
 * <li>[pattern(".+"), name(".*")]} is NOT detected as invalid.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class Header {
    /** Ordered list of header cells. */
    private final List<HeaderCell> sortedCells;
    private final Set<HeaderCell> cells = new HashSet<>();
    private final Map<String, Integer> nameToIndex = new HashMap<>();
    private final Map<Pattern, Integer> patternToIndex = new HashMap<>();
    private final Set<String> patterns = new HashSet<>();
    private final boolean valid;
    private List<String> sortedNames = null;

    public static final Header EMPTY = builder().build();

    /**
     * Checks the validity of this Header.
     *
     * @throws IllegalStateException When this header is invalid.
     */
    private void checkValidity() {
        if (!valid) {
            throw new IllegalStateException("Invalid header (duplicate names or patterns, or name matching a pattern): " + this);
        }
    }

    private Header(Builder builder) {
        boolean v = true;
        int index = 0;
        for (final HeaderCell cell : builder.cells) {
            Checks.isNotNull(cell, "cell");
            if (cells.contains(cell)) {
                v = false;
            } else {
                cells.add(cell);
                if (cell instanceof final NameCell c) {
                    nameToIndex.put(c.getName(), index);
                } else if (cell instanceof final PatternCell c) {
                    patternToIndex.put(c.getPattern(), index);
                    patterns.add(c.getPattern().pattern());
                }
            }
            index++;
        }
        this.sortedCells = Collections.unmodifiableList(builder.cells);

        // Check that no name matches a pattern
        final List<NameCell> names = sortedCells.stream()
                                                .filter(NameCell.class::isInstance)
                                                .map(NameCell.class::cast)
                                                .toList();

        final List<PatternCell> patts = sortedCells.stream()
                                                   .filter(PatternCell.class::isInstance)
                                                   .map(PatternCell.class::cast)
                                                   .toList();

        for (final PatternCell pattern : patts) {
            for (final NameCell name : names) {
                if (pattern.matches(name)) {
                    v = false;
                }
            }
        }
        this.valid = v;
    }

    /**
     * @return {@code true} if this header is valid (it does not contain duplicates).
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @return {@code true} if this Header only contains {@link NameCell names} (no patterns).
     */
    public boolean hasOnlyNames() {
        return patternToIndex.isEmpty();
    }

    /**
     * @return {@code true} if this Header contains {@link PatternCell patterns}.
     */
    public boolean hasPatterns() {
        return !patternToIndex.isEmpty();
    }

    /**
     * @return The number of cells of this Header.<br>
     *         <b>WARNING:</b> The number of columns that match this header may be greater, if it contains patterns.
     */
    public int size() {
        return sortedCells.size();
    }

    /**
     * Returns the cell at a position.
     *
     * @param index The index.
     * @return The cell at {@code index}.
     * @throws IndexOutOfBoundsException When {@code index} is out of range.
     */
    public HeaderCell getCellAt(int index) {
        return sortedCells.get(index);
    }

    /**
     * Returns the name at a position.
     *
     * @param index The index.
     * @return The name at {@code index}.
     * @throws IndexOutOfBoundsException When {@code index} is out of range.
     * @throws ClassCastException When cell at {@code index} is not a name cell.
     */
    public String getNameAt(int index) {
        return ((NameCell) sortedCells.get(index)).getName();
    }

    /**
     * @return An ordered (declaration order) list of all declared cells (even if this header is invalid).
     */
    public List<HeaderCell> getSortedCells() {
        return sortedCells;
    }

    /**
     * @return An ordered (declaration order) list of all declared cells names.
     * @throws ClassCastException When one of the cells is not a name cell.
     */
    public List<String> getSortedNames() {
        // Compute the list the first time if it is valid, every time otherwise
        if (sortedNames == null) {
            final List<String> list = new ArrayList<>();
            for (final HeaderCell cell : sortedCells) {
                list.add(((NameCell) cell).getName());
            }
            sortedNames = list;
        }
        return sortedNames;
    }

    /**
     * @return A set of all declared cells.
     */
    public Set<HeaderCell> getDeclaredCells() {
        return cells;
    }

    public <X extends HeaderCell> Set<X> getDeclaredCells(Class<X> cls) {
        return cells.stream().filter(cls::isInstance).map(cls::cast).collect(Collectors.toSet());
    }

    /**
     * @return The set of names declared in this Header.
     */
    public Set<String> getDeclaredNames() {
        return nameToIndex.keySet();
    }

    /**
     * @return The set of patterns declared in this Header.
     */
    public Set<String> getDeclaredPatterns() {
        return patterns;
    }

    /**
     * Returns {@code true} when a cell is contained in this header (even if this header is invalid).
     *
     * @param cell The cell.
     * @return {@code true} when {@code cell} is contained in this header.
     */
    public boolean contains(HeaderCell cell) {
        return cells.contains(cell);
    }

    /**
     * Return {@code true} when this header contains a {@link NameCell} having a particular name.
     *
     * @param name The name.
     * @return {@code true} when this header contains a {@link NameCell} whose name is {@code name}.
     */
    public boolean contains(String name) {
        return nameToIndex.containsKey(name);
    }

    public boolean containsAll(Collection<HeaderCell> cells) {
        return this.cells.containsAll(cells);
    }

    /**
     * Returns the index of the cell that matches an actual name, or a negative number.
     * <ul>
     * <li>If returned value is {@code >=0}, then there is one matching cell.
     * <li>If returned value is {@code -1}, then there are no matching cells.
     * <li>If returned value is {@code <=-2}, then there are several matching cells (the absolute value of returned value).
     * </ul>
     *
     * @param name The actual name.
     * @return The cell index matching {@code name}, or -1 (no match), or a negative number (2 or more matches).
     * @throws IllegalStateException When this header is invalid.
     */
    public int getMatchingIndex(String name) {
        checkValidity();

        // Start with names
        int index = nameToIndex.getOrDefault(name, -1);

        // Now check patterns, even if an index was found as name
        for (final Map.Entry<Pattern, Integer> entry : patternToIndex.entrySet()) {
            if (entry.getKey().matcher(name).matches()) {
                if (index == -1) {
                    // No matching name or pattern was found
                    index = entry.getValue();
                } else if (index >= 0) {
                    // A matching name or pattern was already found
                    // So there are 2 matches
                    index = -2;
                } else { // <= -2
                    // There are at least 3 matches
                    index--;
                }
            }
        }
        return index;
    }

    /**
     * @param name The actual name.
     * @return The number of cells that match {@code name}.
     * @throws IllegalStateException When this header is invalid.
     */
    public int getMatchingCount(String name) {
        final int index = getMatchingIndex(name);
        if (index >= 0) {
            return 1;
        } else if (index == -1) {
            return 0;
        } else {
            return -index;
        }
    }

    /**
     * Returns {@code true} when this Header contains at least one cell that matches an actual name.
     * <p>
     * <b>WARNING:</b> The result can be {@code true}, but {@code getMatchingCell(name)} can return an empty result
     * if there are several matches. That can happen when header contains patterns.
     *
     * @param name The actual name.
     * @return {@code true} if this Header contains at least one cell that matches {@code name}.
     * @throws IllegalStateException When this header is invalid.
     */
    public boolean matches(String name) {
        return getMatchingCount(name) > 0;
    }

    /**
     * Returns {@code true} when this Header contains exactly one cell that matches an actual name.
     *
     * @param name The actual name.
     * @return {@code true} if this Header contains exactly one cell that matches {@code name}.
     * @throws IllegalStateException When this header is invalid.
     */
    public boolean matchesOne(String name) {
        return getMatchingCount(name) == 1;
    }

    /**
     * Returns the cell that matches an actual name.
     * <p>
     * If no or several cells match {@code name}, {@code null} is returned.
     *
     * @param name The actual name.
     * @return The cell that matches {@code name}, or {@code null}.
     * @throws IllegalStateException When this header is invalid.
     */
    public Optional<HeaderCell> getMatchingCell(String name) {
        final int index = getMatchingIndex(name);
        if (index < 0) {
            return Optional.empty();
        } else {
            return Optional.of(sortedCells.get(index));
        }
    }

    /**
     * Returns {@code true} if this Header and another one have a non-empty intersection.
     * <p>
     * <b>WARNING:</b> matching of a pattern with another pattern is ignored.
     * Matching of name with another name or pattern is checked.
     *
     * @param other The other Header.
     * @return {@code true} if this Header and {@code other} have a non-empty intersection.
     */
    public boolean intersects(Header other) {
        // Are there any identical patterns?
        for (final String pattern : other.getDeclaredPatterns()) {
            if (getDeclaredPatterns().contains(pattern)) {
                return true;
            }
        }

        // Does a local name match any other name or pattern?
        for (final String name : getDeclaredNames()) {
            if (other.matches(name)) {
                return true;
            }
        }

        // Does an other name match any local name or pattern?
        for (final String name : other.getDeclaredNames()) {
            if (this.matches(name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns {@code true} if this Header contains another one.
     * <p>
     * <b>WARNING:</b> equality is used, not matching.
     *
     * @param other The other Header.
     * @return {@code true} if this Header contains {@code other}.
     */
    public boolean contains(Header other) {
        return cells.containsAll(other.getSortedCells());
    }

    @Override
    public int hashCode() {
        return Objects.hash(sortedCells);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Header)) {
            return false;
        }
        final Header other = (Header) object;
        return Objects.equals(sortedCells, other.sortedCells);
    }

    @Override
    public String toString() {
        return sortedCells.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final List<HeaderCell> cells = new ArrayList<>();

        Builder() {
        }

        public Builder name(String name) {
            this.cells.add(HeaderCell.name(name));
            return this;
        }

        public Builder pattern(String regex) {
            this.cells.add(HeaderCell.pattern(regex));
            return this;
        }

        public Builder names(String... names) {
            for (final String name : names) {
                this.cells.add(HeaderCell.name(name));
            }
            return this;
        }

        public Builder names(Collection<String> names) {
            for (final String name : names) {
                this.cells.add(HeaderCell.name(name));
            }
            return this;
        }

        public Builder cells(Header header) {
            this.cells.addAll(header.getSortedCells());
            return this;
        }

        public Builder cells(HeaderCell... cells) {
            Collections.addAll(this.cells, cells);
            return this;
        }

        public Builder cells(List<? extends HeaderCell> cells) {
            this.cells.addAll(cells);
            return this;
        }

        public Builder names(Row row) {
            return names(row.getValues());
        }

        /**
         * Fills this builder with names of a model header, and replaces patterns of the model header
         * with some passed names.
         * <p>
         * Order of model header and substitution is preserved.
         *
         * @param header The model header.
         * @param by The list of names that must be used in place of patterns.
         * @return This Builder.
         */
        public Builder replacePatterns(Header header,
                                       List<String> by) {
            for (final HeaderCell cell : header.getSortedCells()) {
                if (cell.isName()) {
                    this.cells.add(cell);
                } else {
                    for (final String name : by) {
                        if (cell.matches(name)) {
                            this.cells.add(HeaderCell.name(name));
                        }
                    }
                }
            }
            return this;
        }

        public Builder replacePatterns(Header header,
                                       String... by) {
            return replacePatterns(header, List.of(by));
        }

        public Header build() {
            return new Header(this);
        }
    }
}