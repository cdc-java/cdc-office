package cdc.office.tables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cdc.office.tables.HeaderCell.NameCell;
import cdc.office.tables.HeaderCell.PatternCell;
import cdc.util.lang.Checks;

/**
 * Helper class used to map expected header to an actual (possibly invalid) one.
 * <p>
 * This class checks that :
 * <ul>
 * <li>Some expected are missing or not.
 * <li>Some actual names are additional or not.
 * </ul>
 * It does not check that order of actual header matches, in some way, an expected order.
 *
 * @author Damien Carbonne
 */
public final class BasicHeaderMapper {
    /** The expected header. */
    private final Header expectedHeader;
    /** The actual header. Shall not contain patterns. */
    private final Header actualHeader;
    /** The missing expected cells. */
    private final Set<HeaderCell> missingExpectedCells;
    /** The actual mandatory names. */
    private final Set<String> actualExpectedNames;
    /** The actual additional names. */
    private final Set<String> additionalNames;
    /** The actual names corresponding to each expected cell header. */
    private final Map<HeaderCell, List<String>> expectedToActuals = new HashMap<>();

    /**
     * Creates a header mapper from expected and actual headers.
     *
     * @param builder The Builder.
     * @throws IllegalArgumentException When {@code expected} is {@code null} or invalid,<br>
     *             or when {@code actual} is {@code null},<br>
     *             or when an {@code actual} cell matches several expected cells.
     */
    private BasicHeaderMapper(Builder builder) {
        this.expectedHeader = Checks.isNotNull(builder.expected, "expected");
        Checks.isTrue(builder.expected.isValid(), "Invalid expected header.");

        this.actualHeader = Checks.isNotNull(builder.actual, "actual");
        Checks.isTrue(builder.actual.isValid(), "Invalid actual header.");
        Checks.isTrue(builder.actual.hasOnlyNames(), "Actual header can not contain patterns.");

        for (final String name : actualHeader.getDeclaredNames()) {
            final int count = expectedHeader.getMatchingCount(name);
            Checks.isFalse(count > 1, "Actual '{}' matches too many ({}) expected cells.", name, count);
        }

        // Set of expected header cells that are missing
        // Add all expected cells, then remove those that are found
        final Set<HeaderCell> missingExpectedSet = new HashSet<>(builder.expected.getSortedCells());
        // Set of actual names that match expected header cell
        final Set<String> actualExpectedSet = new HashSet<>();

        for (final String name : actualHeader.getDeclaredNames()) {
            final Optional<HeaderCell> matching = expectedHeader.getMatchingCell(name);
            if (matching.isPresent()) {
                missingExpectedSet.remove(matching.get());
                actualExpectedSet.add(name);
                final List<String> list = expectedToActuals.computeIfAbsent(matching.get(), k -> new ArrayList<>());
                list.add(name);
            }
        }
        this.missingExpectedCells = Collections.unmodifiableSet(missingExpectedSet);
        this.actualExpectedNames = Collections.unmodifiableSet(actualExpectedSet);

        final Set<String> additionalSet = new HashSet<>();
        additionalSet.addAll(builder.actual.getDeclaredNames());
        additionalSet.removeAll(this.actualExpectedNames);
        this.additionalNames = Collections.unmodifiableSet(additionalSet);
    }

    /**
     * @return The expected header.
     */
    public Header getExpectedHeader() {
        return expectedHeader;
    }

    /**
     * @return The actual header, possibly invalid.
     */
    public Header getActualHeader() {
        return actualHeader;
    }

    /**
     * @return A set of expected cells.
     */
    public Set<HeaderCell> getExpectedCells() {
        return expectedHeader.getDeclaredCells();
    }

    /**
     * @return A set of missing expected cells.
     */
    public Set<HeaderCell> getMissingExpectedCells() {
        return missingExpectedCells;
    }

    /**
     * @return A set of actual mandatory names.
     */
    public Set<String> getActualExpectedNames() {
        return actualExpectedNames;
    }

    /**
     * @return {@code true} when there are missing mandatory cells.
     */
    public boolean hasMissingExpectedCells() {
        return !missingExpectedCells.isEmpty();
    }

    /**
     * @return {@code true} when all expected cells are there.<br>
     *         <b>Note</b> that mandatory header may however be invalid.
     */
    public boolean hasAllExpectedCells() {
        return missingExpectedCells.isEmpty();
    }

    /**
     * @return A set of additional actual names.<br>
     *         They are neither optional nor mandatory.
     */
    public Set<String> getAdditionalNames() {
        return additionalNames;
    }

    /**
     * @return {@code true} when there are additional actual names.
     *         They are neither optional nor mandatory.
     */
    public boolean hasAdditionalNames() {
        return !getAdditionalNames().isEmpty();
    }

    /**
     * Returns the list of actual names that match an expected header cell.
     * <p>
     * For a {@link NameCell}, the list should be empty or contain 1 value.<br>
     * For a {@link PatternCell}, the list can have any size.
     *
     * @param expected The expected (mandatory of optional) header cell.
     * @return The list of actual names matching {@code expected}.
     * @throws IllegalArgumentException When {@code expected} is {@code null} or unknown.
     */
    public List<String> getActuals(HeaderCell expected) {
        Checks.isNotNull(expected, "expected");
        Checks.isTrue(this.expectedHeader.contains(expected),
                      "Unkown header cell {}",
                      expected);

        return expectedToActuals.getOrDefault(expected, Collections.emptyList());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Header expected;
        private Header actual;

        private Builder() {
        }

        public Builder expected(Header expected) {
            this.expected = expected;
            return this;
        }

        public Builder actual(Header actual) {
            this.actual = actual;
            return this;
        }

        public BasicHeaderMapper build() {
            return new BasicHeaderMapper(this);
        }
    }
}