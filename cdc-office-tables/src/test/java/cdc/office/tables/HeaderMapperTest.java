package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

class HeaderMapperTest {
    @Test
    void test() {
        final Header m = Header.builder().names("M1", "M2").build();
        final Header o = Header.builder().names("O1", "O2").build();
        final Header a = Header.builder().names("M1", "M2", "O1", "O2", "A1").build();

        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(m)
                                                .optional(o)
                                                .actual(a)
                                                .build();

        assertEquals(m, mapper.getMandatoryHeader());
        assertEquals(o, mapper.getOptionalHeader());
        assertEquals(a, mapper.getActualHeader());

        assertEquals(m.getDeclaredCells(), mapper.getExpectedMandatoryCells());
        assertEquals(o.getDeclaredCells(), mapper.getExpectedOptionalCells());
        assertEquals(m.getDeclaredNames(), mapper.getActualMandatoryNames());
        assertEquals(o.getDeclaredNames(), mapper.getActualOptionalNames());

        assertFalse(mapper.hasMissingMandatoryCells());
        assertFalse(mapper.hasMissingOptionalCells());
        assertTrue(mapper.hasAdditionalNames());
        assertSame(0, mapper.getMissingMandatoryCells().size());
        assertSame(0, mapper.getMissingOptionalCells().size());
        assertSame(1, mapper.getAdditionalNames().size());
    }

    @Test
    void testInvalidActual() {
        // Actual can not contain pattern
        final Header m = Header.builder().build();
        final Header o = Header.builder().build();
        final Header a = Header.builder().pattern(".*").build();

        assertThrows(IllegalArgumentException.class,
                     () -> HeaderMapper.builder()
                                       .mandatory(m)
                                       .optional(o)
                                       .actual(a)
                                       .build());
    }

    @Test
    void testInvalidMandatoryOptional1() {
        // Mandatory and optional can not intersect
        final Header m = Header.builder().names("A").build();
        final Header o = Header.builder().names("A").build();
        final Header a = Header.builder().names("A").build();

        assertThrows(IllegalArgumentException.class,
                     () -> HeaderMapper.builder()
                                       .mandatory(m)
                                       .optional(o)
                                       .actual(a)
                                       .build());
    }

    @Test
    void testInvalidMandatoryOptional2() {
        // Mandatory and optional can not intersect
        final Header m = Header.builder().names("A").build();
        final Header o = Header.builder().pattern("A").build();
        final Header a = Header.builder().names("A").build();

        assertThrows(IllegalArgumentException.class,
                     () -> HeaderMapper.builder()
                                       .mandatory(m)
                                       .optional(o)
                                       .actual(a)
                                       .build());
    }

    @Test
    void testInvalidMandatoryOptional3() {
        // Mandatory and optional have an intersection detected with actual
        final Header m = Header.builder().pattern(".*").build();
        final Header o = Header.builder().pattern(".").build();
        final Header a = Header.builder().names("A").build();

        assertThrows(IllegalArgumentException.class,
                     () -> HeaderMapper.builder()
                                       .mandatory(m)
                                       .optional(o)
                                       .actual(a)
                                       .build());
    }

    @Test
    void testExceptions() {
        final Header e = Header.builder().names("M1", "M2").build();
        final Header a = Header.builder().names("M1", "M2", "O1", "O2", "A1").build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         HeaderMapper.builder()
                                     .build();
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         HeaderMapper.builder()
                                     .mandatory(e)
                                     .optional(e)
                                     .actual(a)
                                     .build();
                     });
    }

    @Test
    void testComplex1() {
        final Header m = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A").build();
        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(m)
                                                .actual(a)
                                                .build();
        assertFalse(mapper.hasAllMandatoryCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(HeaderCell.pattern("B-[0-9]+")), mapper.getMissingMandatoryCells());
        assertEquals(Set.of("A"), mapper.getActualMandatoryNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of(), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }

    @Test
    void testComplex2() {
        final Header m = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A", "B-0").build();
        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(m)
                                                .actual(a)
                                                .build();
        assertTrue(mapper.hasAllMandatoryCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(), mapper.getMissingMandatoryCells());
        assertEquals(Set.of("A", "B-0"), mapper.getActualMandatoryNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of("B-0"), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }

    @Test
    void testComplex3() {
        final Header m = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A", "B-0", "B-1").build();
        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(m)
                                                .actual(a)
                                                .build();
        assertTrue(mapper.hasAllMandatoryCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(), mapper.getMissingMandatoryCells());
        assertEquals(Set.of("A", "B-0", "B-1"), mapper.getActualMandatoryNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of("B-0", "B-1"), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }
}