package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.junit.jupiter.api.Test;

class RowsTest {
    @Test
    void testBug55() {
        final Header header = Header.builder().names("A", "B").build();
        final Row row = Row.builder("V0", "V1", "V2").build();
        assertEquals(Row.EMPTY, Rows.filter(row, header, Set.of()));
        assertEquals(Row.EMPTY, Rows.filter(row, header, Set.of("C")));
        assertEquals(Row.builder("V0").build(), Rows.filter(row, header, Set.of("A")));
        assertEquals(Row.builder("V1").build(), Rows.filter(row, header, Set.of("B")));
        assertEquals(Row.builder("V0", "V1").build(), Rows.filter(row, header, Set.of("B", "A")));
    }
}