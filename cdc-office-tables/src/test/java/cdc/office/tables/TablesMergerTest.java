package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TablesMergerTest {
    @Test
    void testEmptyTrailing() {
        final MemoryTableHandler handler = new MemoryTableHandler(false);
        final TablesMerger merger = new TablesMerger(s -> true, handler);

        final String systemId = "SystemId";
        final int tablesCount = 5;
        final int dataCount = 10;

        merger.processBeginTables(systemId);
        for (int i = 0; i < tablesCount; i++) {
            final String tableName = "Sheet#" + i;
            final RowLocation.Builder loc = RowLocation.builder();
            merger.processBeginTable(tableName, -1);
            loc.incrementNumbers(TableSection.HEADER);
            merger.processHeader(Row.builder().addValues("A", "B", "C").build(), loc.build());

            for (int r = 0; r < dataCount; r++) {
                loc.incrementNumbers(TableSection.DATA);
                merger.processData(Row.builder().addValues("a", "b", "c").build(), loc.build());
            }

            merger.processEndTable(tableName);
        }
        merger.processEndTables(systemId);

        assertEquals(tablesCount * dataCount + 1, handler.getRows().size());
    }
}