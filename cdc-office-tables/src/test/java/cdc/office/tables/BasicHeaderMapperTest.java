package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

class BasicHeaderMapperTest {
    @Test
    void test() {
        final Header e = Header.builder().names("E1", "E2").build();
        final Header a = Header.builder().names("E1", "E2", "A1").build();

        final BasicHeaderMapper mapper = BasicHeaderMapper.builder()
                                                          .expected(e)
                                                          .actual(a)
                                                          .build();

        assertEquals(e, mapper.getExpectedHeader());
        assertEquals(a, mapper.getActualHeader());

        assertEquals(e.getDeclaredCells(), mapper.getExpectedCells());
        assertEquals(e.getDeclaredNames(), mapper.getActualExpectedNames());

        assertFalse(mapper.hasMissingExpectedCells());
        assertTrue(mapper.hasAdditionalNames());
        assertSame(0, mapper.getMissingExpectedCells().size());
        assertSame(1, mapper.getAdditionalNames().size());
    }

    @Test
    void testInvalidActual() {
        // Actual can not contain pattern
        final Header e = Header.builder().build();
        final Header a = Header.builder().pattern(".*").build();

        assertThrows(IllegalArgumentException.class,
                     () -> BasicHeaderMapper.builder()
                                            .expected(e)
                                            .actual(a)
                                            .build());
    }

    @Test
    void testInvalidExpected() {
        // Several expected patterns match the same actual
        final Header e = Header.builder().pattern(".*").pattern(".").build();
        final Header a = Header.builder().names("A").build();

        assertThrows(IllegalArgumentException.class,
                     () -> BasicHeaderMapper.builder()
                                            .expected(e)
                                            .actual(a)
                                            .build());
    }

    @Test
    void testExceptions() {
        final Header e = Header.builder().names("E1", "E2").build();
        final Header a = Header.builder().names("E1", "E2", "A1").build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BasicHeaderMapper.builder()
                                          .build();
                     });
    }

    @Test
    void testComplex1() {
        final Header e = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A").build();
        final BasicHeaderMapper mapper = BasicHeaderMapper.builder()
                                                          .expected(e)
                                                          .actual(a)
                                                          .build();
        assertFalse(mapper.hasAllExpectedCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(HeaderCell.pattern("B-[0-9]+")), mapper.getMissingExpectedCells());
        assertEquals(Set.of("A"), mapper.getActualExpectedNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of(), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }

    @Test
    void testComplex2() {
        final Header e = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A", "B-0").build();
        final BasicHeaderMapper mapper = BasicHeaderMapper.builder()
                                                          .expected(e)
                                                          .actual(a)
                                                          .build();
        assertTrue(mapper.hasAllExpectedCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(), mapper.getMissingExpectedCells());
        assertEquals(Set.of("A", "B-0"), mapper.getActualExpectedNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of("B-0"), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }

    @Test
    void testComplex3() {
        final Header e = Header.builder().name("A").pattern("B-[0-9]+").build();
        final Header a = Header.builder().names("A", "B-0", "B-1").build();
        final BasicHeaderMapper mapper = BasicHeaderMapper.builder()
                                                          .expected(e)
                                                          .actual(a)
                                                          .build();
        assertTrue(mapper.hasAllExpectedCells());
        assertFalse(mapper.hasAdditionalNames());
        assertEquals(Set.of(), mapper.getMissingExpectedCells());
        assertEquals(Set.of("A", "B-0", "B-1"), mapper.getActualExpectedNames());
        assertEquals(List.of("A"), mapper.getActuals(HeaderCell.name("A")));
        assertEquals(List.of("B-0", "B-1"), mapper.getActuals(HeaderCell.pattern("B-[0-9]+")));
    }
}