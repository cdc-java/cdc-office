package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class HeaderTest {
    @Test
    void testEmpty() {
        final Header h = Header.builder().build();
        assertTrue(h.isValid());
        assertSame(0, h.size());
        assertEquals(Header.EMPTY, h);
    }

    @Test
    void test1Name() {
        final Header h = Header.builder().names("K1").build();
        assertTrue(h.isValid());
        assertSame(1, h.size());
        assertTrue(h.matches("K1"));
        assertFalse(h.matches("K"));
    }

    @Test
    void test2Names() {
        final Header h = Header.builder().names("K1", "K2").build();
        assertTrue(h.isValid());
        assertSame(2, h.size());
        assertTrue(h.matches("K1"));
        assertTrue(h.matches("K2"));
        assertFalse(h.matches("K"));
        assertSame(0, h.getMatchingIndex("K1"));
        assertSame(1, h.getMatchingIndex("K2"));
        assertSame(-1, h.getMatchingIndex("K"));
    }

    @Test
    void testInvalid() {
        assertFalse(Header.builder().names("K1", "K1").build().isValid());
        assertFalse(Header.builder().name("K").pattern("K").build().isValid());
        assertFalse(Header.builder().name("K").pattern(".").build().isValid());
        assertFalse(Header.builder().name("K").pattern(".*").build().isValid());
        assertFalse(Header.builder().name("K").pattern(".+").build().isValid());
        assertFalse(Header.builder().pattern(".*").pattern(".*").build().isValid());

        assertTrue(Header.builder().pattern(".*").pattern(".+").build().isValid());
    }

    @Test
    void testContains() {
        final Header h12 = Header.builder().names("K1", "K2").build();
        final Header h123 = Header.builder().names("K1", "K2", "K3").build();
        assertTrue(h123.contains(h12));
        assertFalse(h12.contains(h123));
        assertTrue(Header.EMPTY.contains(Header.EMPTY));
        assertTrue(h12.contains(Header.EMPTY));
    }

    @Test
    void testIntersects() {
        final Header h1a = Header.builder().names("K1", "K2").build();
        final Header h1b = Header.builder().names("K1", "K2", "K3").build();
        assertTrue(h1b.intersects(h1a));
        assertFalse(h1a.intersects(Header.EMPTY));
        assertFalse(Header.EMPTY.intersects(Header.EMPTY));

        final Header h21a = Header.builder().pattern(".").build();
        final Header h2b = Header.builder().pattern(".").build();
        assertTrue(h21a.intersects(h2b));

        final Header h3a = Header.builder().pattern(".").build();
        final Header h3b = Header.builder().name("A").build();
        assertTrue(h3a.intersects(h3b));
    }

    @Test
    void testGetMatchingCount() {
        assertEquals(0, Header.builder().name("A").build().getMatchingCount("B"));
        assertEquals(1, Header.builder().name("A").build().getMatchingCount("A"));
        assertEquals(1, Header.builder().pattern(".").build().getMatchingCount("A"));
        assertEquals(2, Header.builder().pattern(".").pattern(".*").build().getMatchingCount("A"));
        assertEquals(3, Header.builder().pattern(".").pattern(".*").pattern(".+").build().getMatchingCount("A"));
    }

    @Test
    void testGetMatchingIndex() {
        assertEquals(-1, Header.builder().name("A").build().getMatchingIndex("B"));
        assertEquals(0, Header.builder().name("A").build().getMatchingIndex("A"));
        assertEquals(0, Header.builder().pattern(".").build().getMatchingIndex("A"));
        assertEquals(-2, Header.builder().pattern(".").pattern(".*").build().getMatchingIndex("A"));
        assertEquals(-3, Header.builder().pattern(".").pattern(".*").pattern(".+").build().getMatchingIndex("A"));
    }

    @Test
    void testReplacePatterns() {
        final Header h = Header.builder().name("A").pattern("B.").name("C").pattern("D.").build();
        assertEquals(Header.builder().names("A", "C").build(),
                     Header.builder().replacePatterns(h).build());
        assertEquals(Header.builder().names("A", "B1", "B2", "C", "D1", "D2").build(),
                     Header.builder().replacePatterns(h, "B1", "D1", "B2", "D2").build());
    }
}