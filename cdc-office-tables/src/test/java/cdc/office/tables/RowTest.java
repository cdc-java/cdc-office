package cdc.office.tables;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class RowTest {
    @Test
    void testEmpty() {
        final Row row = Row.builder().build();
        final Row other = row;
        assertTrue(row.isEmpty());
        assertSame(0, row.size());
        assertSame(null, row.getValue(0));
        assertEquals("[]", row.toString());
        assertEquals(Row.EMPTY, row);
        assertEquals(other, row);
        assertSame(Row.EMPTY.hashCode(), row.hashCode());
        assertNotEquals(row, "foo");
    }

    @Test
    void testOne() {
        final Row row = Row.builder().addValue("foo").build();
        assertFalse(row.isEmpty());
        assertSame(1, row.size());
        assertEquals("foo", row.getValue(0));
        assertSame(null, row.getValue(1));
        assertSame(null, row.getValue(-1));
        assertEquals("['foo']", row.toString());
    }

    @Test
    void testOneNull() {
        final Row row = Row.builder().addValue(null).build();
        assertFalse(row.isEmpty());
        assertSame(1, row.size());
        assertSame(null, row.getValue(0));
        assertSame(null, row.getValue(1));
        assertEquals("[null]", row.toString());
    }

    @Test
    void testFix() {
        final String prefix = "Col";
        final Row row1 = Row.builder().addValue(null).build().fixNullAndDuplicates(prefix);
        assertFalse(row1.hasDuplicateValues());
        assertFalse(row1.hasNullValues());
        assertEquals("Col0", row1.getValue(0));

        final Row row2 = Row.builder().addValue(null).addValue(null).build().fixNullAndDuplicates(prefix);
        assertFalse(row2.hasDuplicateValues());
        assertFalse(row2.hasNullValues());
        assertEquals("Col0", row2.getValue(0));
        assertEquals("Col1", row2.getValue(1));

        final Row row3 = Row.builder().addValue("N").addValue("N").build().fixNullAndDuplicates(prefix);
        assertFalse(row3.hasDuplicateValues());
        assertFalse(row3.hasNullValues());
        assertEquals("N", row3.getValue(0));
        assertEquals("Col0", row3.getValue(1));

        final Row row4 = Row.builder().addValue("A").addValue("B").build();
        assertEquals(row4, row4.fixNullAndDuplicates(prefix));
    }
}